FROM eclipse-temurin:17-jdk-focal as builder

RUN mkdir -p /usr/dev

WORKDIR /usr/dev
COPY . .

RUN chmod +x gradlew && \
    ./gradlew installDist

FROM eclipse-temurin:17-jre-focal
LABEL maintainer="vinarnt@outlook.com"

COPY --from=builder /usr/dev/build/install/kotlin-discord-bot /usr/app

WORKDIR /usr/app
COPY docker-entrypoint.sh .
COPY config.yml.template .
RUN chmod +x docker-entrypoint.sh

ENTRYPOINT /usr/app/docker-entrypoint.sh