#Kotlin Discord Bot

## Gradle

You may have to set the permissions of gradlew before building or running
```bash 
$ sudo chmod +x gradlew
```

### Prerequisites

A [MongoDB](https://docs.mongodb.com/manual/installation/) server is needed for the bot
to run properly.

You need to create a `config.yml` file with this content and complete it according to 
your MongoDB installation and your discord bot token.

```yaml
DISCORD_TOKEN=
DB_HOST=
DB_PORT=
DB_NAME=
DB_USER=
DB_PASSWORD=
```

The path of this file depend on the method you use to build or run the bot. 
You can run it a first time and the error to know where to put it.

### Run

```bash
$ ./gradlew run
```

### Build an archive
The archives will be available in build/distributions
```bash
$ ./gradlew assembleDist
```

### Build a flat install folder with start scripts

The folder will be available in build/install
```bash
$ ./gradlew installDist
```


## Docker

The easiest way to go is with [docker-compose](https://docs.docker.com/compose/install/).

### Setup

* Create a new folder where you want to setup your docker image and enter it.
```bash
$ mkdir discord_bot
$ cd discord_bot
```

* Add a `docker-compose.yml` file with this content
```yaml
version: "3.7"
services:
  bot:
    image: registry.gitlab.com/vinarnt/kotlin-discord-bot
    container_name: "discord_bot"
    restart: unless-stopped
    depends_on:
      - database
    env_file: 
      - .env
    volumes:
      - mongo-data:/data/db
      - ./plugins:plugins:ro

  database:
    image: mongo:4
    container_name: "mongo"
    restart: unless-stopped
    command: mongod --port ${DB_PORT}
    ports:
      - "${DB_PORT}:${DB_PORT}"
    environment:
      MONGO_INITDB_ROOT_USERNAME: ${DB_USER}
      MONGO_INITDB_ROOT_PASSWORD: ${DB_PASSWORD}

volumes:
  mongo-data:
```

* Now create a `.env` file with this content and complete it with your discord bot token
```bash
DISCORD_TOKEN=
DB_HOST=mongo
DB_PORT=27017
DB_NAME=kotlin_discord_bot
DB_USER=
DB_PASSWORD=
```
**Note:** DB_USER and DB_PASSWORD are optionals and if changed, both services will need 
to be recreated to take effect.

### Run

```bash
$ docker-compose up -d
```

### Add plugins

Create a `plugins` folder and add your plugin inside it.

### See logs

```bash
$ docker-compose logs bot
```

### Access the container

```bash
$ docker-compose exec bot sh
```