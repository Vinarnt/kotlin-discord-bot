package com.omega.discord.bot

import discord4j.common.util.Snowflake
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.Role
import discord4j.core.`object`.entity.User
import org.slf4j.LoggerFactory
import java.util.function.Consumer


fun getRoleFromId(id: Long): Role? {
    BotManager.client.guilds
            .collectList()
            .block()!!
            .forEach { guild ->
                val role = BotManager.gateway.getRoleById(Snowflake.of(guild.id()), Snowflake.of(id)).block()
                role.let {
                    return it
                }
            }
    return null
}

fun getGuildFromId(id: Long): Guild? =
        BotManager.gateway
                .getGuildById(Snowflake.of(id))
                .block()

fun getUserFromId(id: Long): User? =
        BotManager.gateway.getUserById(Snowflake.of(id)).block()

fun getMessageFromId(channelId: Long, messageId: Long): Message? =
        BotManager.gateway.getMessageById(Snowflake.of(channelId), Snowflake.of(messageId)).block()

class SubscriberExceptionHandler : Consumer<Throwable> {
    companion object {
        private val logger = LoggerFactory.getLogger("SubscriberExceptionHandler")
    }

    override fun accept(t: Throwable) {
        logger.error("An exception has been throw in a subscriber", t)
    }
}