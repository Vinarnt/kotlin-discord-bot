package com.omega.discord.bot

import com.omega.discord.bot.command.CommandRegistry
import com.omega.discord.bot.command.SlashCommandRegister
import com.omega.discord.bot.database.DatabaseManager
import com.omega.discord.bot.listener.ChatInputInteractionSubscriber
import com.omega.discord.bot.listener.FunMentionSubscriber
import com.omega.discord.bot.listener.MessageCommandSubscriber
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.service.AgreementService
import com.sksamuel.hoplite.ConfigException
import discord4j.core.DiscordClient
import discord4j.core.DiscordClientBuilder
import discord4j.core.GatewayDiscordClient
import discord4j.core.`object`.entity.ApplicationInfo
import discord4j.core.event.domain.guild.GuildCreateEvent
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.core.event.domain.lifecycle.ReadyEvent
import discord4j.core.event.domain.message.MessageCreateEvent
import discord4j.gateway.intent.IntentSet
import discord4j.rest.http.client.ClientException
import discord4j.rest.request.RouteMatcher
import discord4j.rest.response.ResponseFunction
import discord4j.rest.route.Routes
import kotlinx.coroutines.reactor.mono
import org.pf4j.DefaultPluginManager
import org.pf4j.PluginManager
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono
import reactor.retry.Retry
import reactor.retry.RetryContext
import java.time.Duration
import java.util.function.Predicate


object BotManager {
    val logger: Logger = LoggerFactory.getLogger("Main")

    lateinit var client: DiscordClient
        private set
    lateinit var gateway: GatewayDiscordClient
        private set
    lateinit var applicationInfo: ApplicationInfo
        private set
    lateinit var pluginManager: PluginManager
        private set

    fun start() {
        try {
            // Load bot config
            BotConfiguration.load()

            client = DiscordClientBuilder.create(BotConfiguration.get().token)
                .apply {
                    onClientResponse(ResponseFunction.emptyIfNotFound())
                    // bad requests (400) while adding reactions will be suppressed
                    onClientResponse(
                        ResponseFunction.emptyOnErrorStatus(
                            RouteMatcher.route(Routes.REACTION_CREATE),
                            400
                        )
                    )
                    // server error (500) while creating a message will be retried, with backoff, until it succeeds
                    onClientResponse(
                        ResponseFunction.retryWhen(
                            RouteMatcher.route(Routes.MESSAGE_CREATE),
                            Retry.onlyIf<Predicate<RetryContext<Any>>>(ClientException.isRetryContextStatusCode(500))
                                .exponentialBackoffWithJitter(Duration.ofSeconds(2), Duration.ofSeconds(10))
                        )
                    )
                        // wait 1 second and retry any server error (500)
                        .onClientResponse(ResponseFunction.retryOnceOnErrorStatus(500))
                }
                .build()

            // Init database manager
            DatabaseManager

            this.gateway = client.gateway()
                .withEventDispatcher { dispatcher ->
                    mono {
                        dispatcher.on(MessageCreateEvent::class.java, MessageCommandSubscriber()).subscribe()
                        dispatcher.on(MessageCreateEvent::class.java, FunMentionSubscriber()).subscribe()
                        dispatcher.on(ChatInputInteractionEvent::class.java, ChatInputInteractionSubscriber())
                            .subscribe()
                        dispatcher.on(ReadyEvent::class.java)
                            .doOnNext {
                                this@BotManager.applicationInfo = it.client.applicationInfo.block()!!
                                logger.info("Application info set to ${this@BotManager.applicationInfo.name}")
                            }
                            .map { event: ReadyEvent -> event.guilds.size }
                            .doOnNext {
                                logger.info("All shards fully ready ($it guilds)")
                            }
                            .subscribe()
                        dispatcher
                            .on(GuildCreateEvent::class.java)
                            .doOnNext {
                                val guild = it.guild
                                logger.info("Joined guild ${guild.name}")
                                SlashCommandRegister.registerCommandsForGuild(guild)
                            }.subscribe()
                    }
                }
                .login()
                .doOnSuccess {
                    logger.info("Connected to gateway")
                }
                .block()!!

            // Load modules
            pluginManager = DefaultPluginManager().apply {
                loadPlugins()
                startPlugins()
            }

            // Init agreement service
            AgreementService

            // Init permissions manager
            PermissionManager

            // Init guild properties manager
            GuildPropertyManager

            // Init command registry
            CommandRegistry

            this.gateway.onDisconnect().block()
        } catch (e: ConfigException) {
            logger.error(e.message)
        } catch (e: Exception) {
            logger.error("Something went wrong when loading bot", e)
        }
    }
}