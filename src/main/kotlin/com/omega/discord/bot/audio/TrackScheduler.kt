package com.omega.discord.bot.audio

import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.player.event.AudioEventAdapter
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import com.sedmelluq.discord.lavaplayer.track.AudioTrackEndReason
import java.util.*

class TrackScheduler(private val audioPlayer: AudioPlayer) : AudioEventAdapter() {
    private val queue: Deque<AudioTrack> = ArrayDeque()

    init {
        audioPlayer.addListener(this)
    }

    fun queue(track: AudioTrack) {
        queue.offer(track)
        if (audioPlayer.playingTrack == null)
            nextTrack()
    }

    fun queue(tracks: List<AudioTrack>) {
        for (track in tracks)
            queue(track)
    }

    fun getQueuedTracks(range: IntRange): List<AudioTrack> {
        val list: MutableList<AudioTrack> = mutableListOf()
        val finalRange: IntRange = if (range.last > queue.size - 1)
            range.first until queue.size
        else
            range

        if (queue.size > 0)
            finalRange.mapTo(list) { queue.elementAt(it) }

        return list
    }

    private fun nextTrack() = audioPlayer.startTrack(queue.poll(), false)

    /**
     * Skip the currently playing track and remove count - 1 tracks from the queue
     * @return the number of tracks removed from the queue
     */
    fun skip(count: Int): Int {
        val currentTrack = audioPlayer.playingTrack
        val minusCount = if (currentTrack != null) 1 else 0
        val queueSize = queue.size
        val finalCount = Math.min(queueSize, count - minusCount)

        queue.removeAll(getQueuedTracks(0 until finalCount))
        nextTrack()

        return finalCount + minusCount
    }

    /**
     * Remove all tracks from the queue.
     */
    fun clear() {
        queue.clear()
    }

    /**
     * @return the number of tracks in queue
     */
    fun queueSize(): Int = queue.size

    override fun onTrackEnd(player: AudioPlayer, track: AudioTrack, endReason: AudioTrackEndReason) {
        if (endReason.mayStartNext)
            nextTrack()
    }
}