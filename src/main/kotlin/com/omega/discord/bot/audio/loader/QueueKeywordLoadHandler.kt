package com.omega.discord.bot.audio.loader

import com.omega.discord.bot.audio.GuildAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.tools.FriendlyException
import com.sedmelluq.discord.lavaplayer.track.AudioPlaylist
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import discord4j.core.`object`.entity.Message
import org.slf4j.Logger
import org.slf4j.LoggerFactory


class QueueKeywordLoadHandler(private val manager: GuildAudioPlayerManager, private val message: Message) : AudioLoadHandler() {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    override fun trackLoaded(track: AudioTrack) {
        super.trackLoaded(track)
        message.edit { spec ->
            spec.setContent("Added track ${track.info.title} to queue")
        }.block()
        manager.scheduler.queue(track)
    }

    override fun playlistLoaded(playlist: AudioPlaylist) {
        super.playlistLoaded(playlist)

        val track = playlist.tracks.first()
        message.edit { spec -> spec.setContent("Added track ${track.info.title} to queue") }.block()
        manager.scheduler.queue(track)
    }

    override fun noMatches() {
        super.noMatches()

        message.edit { spec -> spec.setContent("No track(s) found") }.block()
    }

    override fun loadFailed(exception: FriendlyException?) {
        super.loadFailed(exception)

        message.edit { spec -> spec.setContent("Failed to load track(s)") }.block()
        logger.error("Failed to load track(s)", exception)
    }
}