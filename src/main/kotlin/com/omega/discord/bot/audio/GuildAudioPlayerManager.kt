package com.omega.discord.bot.audio

import com.sedmelluq.discord.lavaplayer.player.AudioPlayerManager
import discord4j.voice.VoiceConnection

class GuildAudioPlayerManager(val manager: AudioPlayerManager) {

    val audioPlayer = manager.createPlayer()!!
    val scheduler: TrackScheduler = TrackScheduler(audioPlayer)
    var voiceConnection: VoiceConnection? = null

    init {
        audioPlayer.volume = 20
    }

    fun getAudioProvider(): AudioProvider {
        return AudioProvider(audioPlayer)
    }
}