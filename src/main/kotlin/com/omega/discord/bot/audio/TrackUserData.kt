package com.omega.discord.bot.audio

import discord4j.core.`object`.entity.User


data class TrackUserData(val skipVotes: MutableSet<User> = mutableSetOf())