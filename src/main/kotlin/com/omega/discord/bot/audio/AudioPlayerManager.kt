package com.omega.discord.bot.audio

import com.sedmelluq.discord.lavaplayer.player.DefaultAudioPlayerManager
import com.sedmelluq.discord.lavaplayer.source.AudioSourceManagers
import com.sedmelluq.discord.lavaplayer.source.youtube.YoutubeAudioSourceManager
import com.sedmelluq.discord.lavaplayer.track.playback.NonAllocatingAudioFrameBuffer
import discord4j.core.`object`.entity.Guild


object AudioPlayerManager {
    private val primaryManager: DefaultAudioPlayerManager = DefaultAudioPlayerManager()
    private val managers: MutableMap<Guild, GuildAudioPlayerManager> = mutableMapOf()

    init {
        primaryManager.configuration.setFrameBufferFactory(::NonAllocatingAudioFrameBuffer)
        AudioSourceManagers.registerRemoteSources(primaryManager)

        val ytSource: YoutubeAudioSourceManager = primaryManager.source(YoutubeAudioSourceManager::class.java)
        ytSource.setPlaylistPageCount(20)
    }

    fun getAudioManager(guild: Guild): GuildAudioPlayerManager {
        guild.let {
            var audioPlayerManager: GuildAudioPlayerManager? = managers[it]
            if (audioPlayerManager == null) {
                audioPlayerManager = GuildAudioPlayerManager(primaryManager)
                managers[it] = audioPlayerManager
            }

            return audioPlayerManager
        }
    }
}