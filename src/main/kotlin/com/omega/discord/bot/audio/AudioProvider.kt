package com.omega.discord.bot.audio

import com.sedmelluq.discord.lavaplayer.format.StandardAudioDataFormats
import com.sedmelluq.discord.lavaplayer.player.AudioPlayer
import com.sedmelluq.discord.lavaplayer.track.playback.MutableAudioFrame
import java.nio.ByteBuffer
import discord4j.voice.AudioProvider as Discord4JAudioProvider


class AudioProvider(private val audioPlayer: AudioPlayer)
    : Discord4JAudioProvider(
        ByteBuffer.allocate(StandardAudioDataFormats.DISCORD_OPUS.maximumChunkSize())
) {

    private val frame = MutableAudioFrame()

    init {
        frame.setBuffer(buffer)
    }

    override fun provide(): Boolean =
            audioPlayer.provide(frame)
                    .also { didProvide: Boolean -> if (didProvide) buffer.flip() }
}