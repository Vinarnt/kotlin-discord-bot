package com.omega.discord.bot.permission

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.omega.discord.bot.database.impl.jackson.JsonDBReference
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.User
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId


@JsonPropertyOrder("id", "guild", "group", "user", "permissions")
data class User(
    @BsonId var id: ObjectId? = null,

    var guild: Guild,

    @JsonDBReference var group: Group?,

    var user: User,
    val permissions: MutableMap<String, PermissionOverride> = mutableMapOf()
) {
    /**
     * Change a permission for this user
     * @param permission the permission to change
     * @param override the permission override to set for this permission
     */
    fun set(permission: String, override: PermissionOverride) {
        if (override == PermissionOverride.INHERIT) {
            permissions.remove(permission)
        } else {
            permissions[permission] = override
        }
    }

    /**
     * Check if the user has the permission
     * @param permission permission to check for
     * @return true if he has permission, false otherwise
     */
    fun has(permission: String): Boolean {
        return (group?.contains(permission) ?: false && permissions[permission] != PermissionOverride.REMOVE) ||
                permissions[permission] == PermissionOverride.ADD
    }
}