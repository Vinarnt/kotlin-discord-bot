package com.omega.discord.bot.permission

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.database.DatabaseManager
import discord4j.core.`object`.entity.Guild
import discord4j.core.event.domain.guild.GuildCreateEvent
import discord4j.core.event.domain.guild.GuildDeleteEvent
import reactor.core.publisher.Mono
import discord4j.core.`object`.entity.User as Discord4JUser
import discord4j.rest.util.Permission as Discord4JPermission


object PermissionManager {
    private val permissions: MutableMap<Guild, GuildPermissions> = hashMapOf()

    init {
        BotManager.gateway.apply {
            on(GuildCreateEvent::class.java) { event ->
                val guild = event.guild
                val groups = DatabaseManager.groupDAO.findFor(guild)
                val users = DatabaseManager.userDAO.findFor(guild)

                permissions[guild] = GuildPermissions(guild, groups, users)
                Mono.empty<GuildCreateEvent>()
            }.subscribe()
            on(GuildDeleteEvent::class.java) { event ->
                permissions.remove(event.guild.get())
                Mono.empty<GuildDeleteEvent>()
            }.subscribe()
        }

        Permission.values().forEach {
            PermissionRegistry.register(it)
        }
    }

    /**
     * Set permission on a user
     * @param guild the targeted guild
     * @param user user to set permission
     * @param permission permission to edit
     * @param override add, remove or inherit permission override
     */
    fun setPermission(guild: Guild, user: Discord4JUser, permission: BasePermission, override: PermissionOverride) {
        val userPerms = getUserPermissions(guild, user)
        userPerms.set(permission.key, override)

        if (userPerms.id == null)
            DatabaseManager.userDAO.insert(userPerms)
        else
            DatabaseManager.userDAO.updatePermission(userPerms, permission.key, override)
    }

    /**
     * Get permissions of a user.
     * @param guild the targeted guild
     * @param user user to get permissions for
     */
    fun getPermissions(guild: Guild, user: Discord4JUser) = getUserPermissions(guild, user)

    /**
     * Add permissions to a group.
     * @param guild the targeted guild
     * @param groupName name of the group
     * @param permission permission to add
     * @return true if permission has been added, false if the group was not found
     */
    fun addPermission(guild: Guild, groupName: String, permission: BasePermission): Boolean {
        val group = getGuildPermissions(guild).add(groupName.lowercase(), permission)
        return if (group != null) {
            DatabaseManager.groupDAO.addPermission(group, permission.key)
            true
        } else false
    }

    /**
     * Add permissions to a group.
     * @param guild the targeted guild
     * @param groupName name of the group
     * @param permission permission to remove
     * @return true if permission has been added, false if the group was not found
     */
    fun removePermission(guild: Guild, groupName: String, permission: BasePermission): Boolean {
        val group = getGuildPermissions(guild).remove(groupName.lowercase(), permission)
        return if (group != null) {
            DatabaseManager.groupDAO.removePermission(group, permission.key)
            true
        } else false
    }

    /**
     * Get permissions of a group.
     * @param guild the targeted guild
     * @param groupName group to get permissions for
     */
    fun getPermissions(guild: Guild, groupName: String): Group? =
        getGuildPermissions(guild).get(groupName.lowercase())

    /**
     * Add a group.
     * @param guild the targeted guild
     * @param name name of the new group
     * @return @return the created group, null if it already exists
     */
    fun addGroup(guild: Guild, name: String): Group? {
        val group = getGuildPermissions(guild).addGroup(name.lowercase())

        if (group != null)
            DatabaseManager.groupDAO.insert(group)

        return group
    }

    /**
     * Remove a group.
     * @param guild the targeted guild
     * @param name name of the group to remove
     * @return true if group has been removed, false otherwise
     */
    fun removeGroup(guild: Guild, name: String): Boolean {
        val group = getGuildPermissions(guild).removeGroup(name.lowercase())

        if (group != null)
            DatabaseManager.groupDAO.delete(group)

        return group != null
    }

    /**
     * Get the list of groups
     * @param guild the targeted guild
     */
    fun getGroups(guild: Guild): Collection<Group> = getGuildPermissions(guild).getGroups()

    /**
     * Set the group of a user.
     * @param guild the targeted guild
     * @param user user to set group to
     * @param groupName the group to set
     * @return true if group set successfully, false otherwise
     */
    fun setGroup(guild: Guild, user: Discord4JUser, groupName: String): Boolean {
        val userPerm = getGuildPermissions(guild).setGroup(user, groupName.lowercase())
        if (userPerm != null) {
            if (userPerm.id === null)
                DatabaseManager.userDAO.insert(userPerm)

            DatabaseManager.userDAO.updateGroup(userPerm)
        }

        return userPerm != null
    }

    fun hasPermission(guild: Guild, user: Discord4JUser, permission: BasePermission): Boolean =
        BotManager.applicationInfo.owner.block() == user // Used for easier maintenance
                || guild.getMemberById(user.id).block()!!.basePermissions.block()!!
            .contains(Discord4JPermission.ADMINISTRATOR)
                || getGuildPermissions(guild).hasPermission(user, permission)

    private fun getGuildPermissions(guild: Guild): GuildPermissions =
        permissions[guild] ?: throw IllegalAccessException("Permissions for guild ${guild.name} not found")

    private fun getUserPermissions(guild: Guild, user: Discord4JUser): User = getGuildPermissions(guild).get(user)
}