package com.omega.discord.bot.permission


object PermissionRegistry {
    private val permissions = mutableMapOf<String, BasePermission>()

    fun register(permission: BasePermission) {
        this.permissions[permission.key] = permission
    }

    fun unregister(permission: BasePermission) {
        permissions.remove(permission.key)
    }

    fun get(key: String): BasePermission? = permissions[key]

    fun getAll(): List<BasePermission> = permissions.values.toList()
}