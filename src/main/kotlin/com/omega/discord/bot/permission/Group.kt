package com.omega.discord.bot.permission

import com.fasterxml.jackson.annotation.JsonIdentityInfo
import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.ObjectIdGenerators
import discord4j.core.`object`.entity.Guild
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId


@JsonPropertyOrder("id", "guild", "name", "properties")
@JsonIdentityInfo(
    generator = ObjectIdGenerators.None::class,
    property = "id"
)
data class Group(
    @BsonId
    var id: ObjectId? = null,

    var guild: Guild,
    var name: String,
    val permissions: MutableSet<String> = hashSetOf()
) {
    /**
     * Add a permission to this group
     * @param permission the permission to add
     * @return true if the permission is already set, false otherwise
     */
    fun add(permission: String): Boolean = permissions.add(permission)

    /**
     * Remove a permission from this group
     * @param permission the permission to remove
     * @return true if the permission has been removed, false if the permission was not already set
     */
    fun remove(permission: String): Boolean = permissions.remove(permission)

    /**
     * Check if this group have the permission.
     * @return true if this group have the permission, false otherwise
     */
    fun contains(permission: String): Boolean = permissions.contains(permission)
}