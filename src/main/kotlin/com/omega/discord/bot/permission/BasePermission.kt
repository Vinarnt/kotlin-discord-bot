package com.omega.discord.bot.permission


interface BasePermission {

    val key: String

    fun get(key: String): Permission? = Permission.values().find { it.key.contentEquals(key) }
}