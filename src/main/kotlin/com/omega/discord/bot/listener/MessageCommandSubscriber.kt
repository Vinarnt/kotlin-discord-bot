package com.omega.discord.bot.listener

import com.omega.discord.bot.command.TextualCommandHandler
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.service.MessageSender
import discord4j.core.event.domain.message.MessageCreateEvent
import org.reactivestreams.Publisher
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono

class MessageCommandSubscriber : java.util.function.Function<MessageCreateEvent, Publisher<MessageCreateEvent>> {
    private val logger: Logger = LoggerFactory.getLogger(MessageCommandSubscriber::class.java)

    override fun apply(event: MessageCreateEvent): Publisher<MessageCreateEvent> {
        val message = event.message
        val messageContent = message.content ?: ""

        val guild = event.guild.block()
        val prefix =
            guild?.let {
                GuildPropertyManager.get(guild, GuildProperty.COMMAND_PREFIX).value as String
            } ?: "!"

        if (messageContent.startsWith(prefix)) {
            var indexEndOfCommandName: Int = messageContent.indexOfFirst { it == ' ' }
            if (indexEndOfCommandName == -1) {
                indexEndOfCommandName = messageContent.length
            }

            val commandName: String = messageContent.substring(1, indexEndOfCommandName)
            try {
                TextualCommandHandler.handle(commandName, message)
            } catch (e: Exception) {
                logger.error("Unhandled error occurred while handling command $commandName", e)
                MessageSender.sendMessage(message.channel, "Error occurred")
            }
        }

        return Mono.empty()
    }
}