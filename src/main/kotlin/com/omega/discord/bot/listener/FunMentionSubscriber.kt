package com.omega.discord.bot.listener

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.service.MessageSender
import discord4j.core.event.domain.message.MessageCreateEvent
import org.litote.kmongo.util.idValue
import org.reactivestreams.Publisher
import reactor.core.publisher.Mono


class FunMentionSubscriber : java.util.function.Function<MessageCreateEvent, Publisher<MessageCreateEvent>> {
    override fun apply(event: MessageCreateEvent): Publisher<MessageCreateEvent> {
        val selfId = BotManager.gateway.selfId
        val message = event.message
        val content = message.content ?: ""
        val hasHeart = content.trim().endsWith("❤️")

        if (
                message.userMentionIds.contains(selfId)
                && hasHeart
        ) {
            val member = event.member
            if (member.isPresent)
                MessageSender.sendMessage(event.message.channel, "${event.member.get().mention} :heart:")
        }

        return Mono.empty()
    }
}