package com.omega.discord.bot.listener

import com.omega.discord.bot.command.SlashCommandHandler
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import org.reactivestreams.Publisher
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono

class ChatInputInteractionSubscriber :
    java.util.function.Function<ChatInputInteractionEvent, Publisher<ChatInputInteractionEvent>> {
    private val logger: Logger = LoggerFactory.getLogger(ChatInputInteractionSubscriber::class.java)

    override fun apply(event: ChatInputInteractionEvent): Publisher<ChatInputInteractionEvent> {
        try {
            SlashCommandHandler.handle(event)
        } catch (e: Exception) {
            logger.error("Unhandled error occurred while handling slash command ${event.commandName}", e)
            event.interactionResponse.createFollowupMessage("Error occurred").block()
        }
        return Mono.empty()
    }
}