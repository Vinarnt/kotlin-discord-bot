package com.omega.discord.bot.command.impl.common

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.command.CommandRegistry
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.*
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.entity.channel.PrivateChannel
import java.util.*


class HelpCommand : TextualCommand() {
    override val name: String = "help"
    override val aliases: Array<String> = arrayOf("h")
    override val usage: String = "**help** - Print all available commands\n" +
            "**help <commandName>** - Print usage of a command"
    override val permission: Permission? = null
    override val allowPrivate: Boolean = true
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        if (args.isNotEmpty())
            commandHelp(author, args[0].lowercase(Locale.getDefault()))
        else
            commandHelp(channel, author)
    }

    private fun commandHelp(channel: MessageChannel, author: User) {
        val builder = StringBuilder("Use !help <commandName> to get a command usage\n\n**Available commands")
        val isPrivate = channel is PrivateChannel

        if (!isPrivate)
            builder.append(" for server ${(channel as GuildMessageChannel).guild.block()!!.name}")

        builder.append(" :**\n```")

        CommandRegistry.get()
            .filterIsInstance<TextualCommand>()
            .filter {
                // Check permission
                it.ownerOnly && author == BotManager.applicationInfo.owner.block()
                        || it.permission == null && !it.ownerOnly
                        || it.permission == null
                        || !isPrivate && author == (channel as GuildMessageChannel).guild.block()!!.owner.block()
                        || if (isPrivate) true
                else PermissionManager.hasPermission(
                    (channel as GuildMessageChannel).guild.block()!!,
                    author,
                    it.permission!!
                )
            }
            .sortedBy { it.name }
            .forEach { command ->
                builder.append(command.name)
                command.aliases?.forEach { builder.append(", ").append(it) }
                builder.append('\n')
            }
        builder.append("```")

        MessageSender.sendPrivateMessage(author, builder.toString())
    }

    private fun commandHelp(author: User, commandName: String) {
        val command: TextualCommand? = CommandRegistry.get(commandName) as TextualCommand?
        if (command != null) {
            val builder = StringBuilder("Usage of command ${command.name} :\n\n")
            builder.append(command.usage)

            MessageSender.sendPrivateMessage(author, builder.toString())
        } else {
            MessageSender.sendPrivateMessage(author, "No command with name $commandName found")
        }
    }
}