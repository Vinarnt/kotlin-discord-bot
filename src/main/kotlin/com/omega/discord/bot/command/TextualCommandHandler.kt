package com.omega.discord.bot.command

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.exception.MissingArgumentException
import com.omega.discord.bot.exception.MissingPermissionException
import com.omega.discord.bot.ext.getNameAndDiscriminator
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.entity.channel.PrivateChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object TextualCommandHandler {
    private val logger: Logger = LoggerFactory.getLogger(TextualCommandHandler.javaClass)

    fun handle(commandName: String, message: Message) {
        val channel: MessageChannel = message.channel.block()!!
        val author: User = message.author.get()
        val command: TextualCommand? = CommandRegistry.get(commandName)?.let { if (it is TextualCommand) it else null }

        if (command == null) {
            logger.info("Command $commandName not found")
            message.addReaction(ReactionEmoji.unicode("⁉")).block()
        } else {
            val permission: BasePermission? = command.permission
            if (
                channel is PrivateChannel
                || (command.ownerOnly && message.author.get() == BotManager.applicationInfo.owner.block())
                || permission == null && !command.ownerOnly
                || permission != null && PermissionManager.hasPermission(
                    message.guild.block()!!,
                    message.author.get(),
                    permission
                )
            ) {
                if (!command.allowPrivate && channel is PrivateChannel) {
                    message.addReaction(ReactionEmoji.unicode("\uD83D\uDEB7")).block()
                } else {
                    try {
                        command.executeInternal(author, channel, message,
                            message.content
                                .split(' ')
                                .drop(1)
                                .filter { it.isNotBlank() }
                                .map { it }
                        )
                        if (channel !is PrivateChannel) message.delete().block()
                    } catch (e: MissingArgumentException) {
                        message.addReaction(ReactionEmoji.unicode("‼")).block()
                        MessageSender.sendPrivateMessage(message.author.get(), e.message!!)
                    } catch (e: MissingPermissionException) {
                        logger.info(e.message)
                        message.addReaction(ReactionEmoji.unicode("⛔")).block()
                    } catch (e: Exception) {
                        logger.warn("Command execution failed", e)
                        message.addReaction(ReactionEmoji.unicode("\uD83C\uDD98")).block()
                    }
                }
            } else {// No permission
                logger.info("User ${author.getNameAndDiscriminator()} doesn't have permission ${command.permission} to run command $commandName")
                message.addReaction(ReactionEmoji.unicode("⛔")).block()
            }
        }
    }
}