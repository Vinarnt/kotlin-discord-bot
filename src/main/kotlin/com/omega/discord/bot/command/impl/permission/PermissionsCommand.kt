package com.omega.discord.bot.command.impl.permission

import com.omega.discord.bot.command.SlashCommand
import com.omega.discord.bot.ext.getNameAndDiscriminator
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.Group
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.permission.PermissionOverride
import com.omega.discord.bot.permission.PermissionRegistry
import com.omega.discord.bot.permission.User
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.`object`.entity.Guild
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest


class PermissionsCommand : SlashCommand() {
    override val name: String = "permissions"
    override val permission: Permission = Permission.COMMAND_PERMISSIONS
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(event: ChatInputInteractionEvent) {
        event.deferReply().withEphemeral(true).doOnSuccess {
            val rootOption = event.options.first()
            when (rootOption.name) {
                "list" -> getGlobalPermissionList(event)
                "user" -> {
                    when (rootOption.options.first().name) {
                        "list" -> getUserPermissionList(event)
                        "add" -> editUserPermission(event, true)
                        "remove" -> editUserPermission(event, false)
                        "setgroup" -> setGroup(event)
                    }
                }

                "group" -> {
                    val groupOption = rootOption.options.first()
                    when (groupOption.name) {
                        "list" -> {
                            val groupName = groupOption.getOption("group").orElse(null)?.value?.get()?.asString()

                            if (groupName === null) getGroupList(event) else getGroupPermissionList(event, groupName)
                        }

                        "create" -> editGroup(event, true)
                        "delete" -> editGroup(event, false)
                        "add" -> editGroupPermission(event, true)
                        "remove" -> editGroupPermission(event, false)
                    }
                }

                "check" -> testPerm(event)
            }
        }.block()
    }

    private fun testPerm(event: ChatInputInteractionEvent) {
        val guild = event.interaction.guild.block()!!
        val rootOption = event.getOption("check").get()
        val user = rootOption.getOption("user").get().value.get().asUser().block()!!
        val permissionKey = rootOption.getOption("permission").get().value.get().asString()
        val permission = PermissionRegistry.get(permissionKey)

        if (permission === null)
            event.interactionResponse.createFollowupMessage("Permission $permissionKey not found").block()
        else {
            val hasPermission = PermissionManager.hasPermission(guild, user, permission)

            event.interactionResponse.createFollowupMessage(
                buildString {
                    append("User ")
                    append(user.mention)
                    if (hasPermission) append(" has permission ") else append(" does not have permission ")
                    append(permission.key)
                }
            ).block()
        }

    }

    private fun getGlobalPermissionList(event: ChatInputInteractionEvent) {
        buildString {
            appendLine("```Available permissions :")
            appendLine()
            PermissionRegistry.getAll().forEach { appendLine(it.key) }
            append("```")
        }.also {
            event.interactionResponse.createFollowupMessage(it).block()
        }
    }

    private fun getUserPermissionList(event: ChatInputInteractionEvent) {
        val rootOption = event.getOption("user").get().getOption("list").get()
        val user = rootOption.getOption("user").get().value.get().asUser().block()!!
        val permissions: MutableSet<String> = mutableSetOf()
        val guild = event.interaction.guild.block()!!

        val userPerms: User = PermissionManager.getPermissions(guild, user)
        val group = userPerms.group

        // Populate permissions from group
        group?.permissions?.let { permissions.addAll(it) }

        // Apply user permission overrides
        userPerms.permissions.forEach { entry ->
            if (entry.value == PermissionOverride.REMOVE)
                permissions.remove(entry.key)
            else if (entry.value == PermissionOverride.ADD)
                permissions.add(entry.key)
        }

        buildString {
            appendLine("```Permissions for user ${user.getNameAndDiscriminator()} (group: ${userPerms.group?.name ?: "none"}) :")
            appendLine()
            permissions.forEach { appendLine(it) }
            append("```")
        }.also {
            event.interactionResponse.createFollowupMessage(it).block()
        }
    }

    private fun getGroupPermissionList(event: ChatInputInteractionEvent, groupName: String) {
        val guild = event.interaction.guild.block()!!
        val permissions = PermissionManager.getPermissions(guild, groupName)?.permissions

        if (permissions != null) {
            buildString {
                appendLine("```Permissions for group $groupName :")
                appendLine()
                permissions.forEach { appendLine(it) }
                append("```")
            }.also {
                event.interactionResponse.createFollowupMessage(it).block()
            }
        } else {
            event.interactionResponse.createFollowupMessage("Group $groupName not found").block()
        }
    }

    private fun editUserPermission(
        event: ChatInputInteractionEvent,
        add: Boolean
    ) {
        val guild: Guild = event.interaction.guild.block()!!
        val rootOption =
            event.getOption("user").get().getOption(if (add) "add" else "remove")
                .get()
        val user = rootOption.getOption("user").get().value.get().asUser().block()!!
        val permissionKey = rootOption.getOption("permission").get().value.get().asString()
        val permission: BasePermission? = PermissionRegistry.get(permissionKey)

        if (permission === null)
            event.interactionResponse.createFollowupMessage("Permission $permissionKey not found").block()
        else {
            PermissionManager.setPermission(
                guild,
                user,
                permission,
                if (add) PermissionOverride.ADD else PermissionOverride.REMOVE
            )

            event.interactionResponse
                .createFollowupMessage(
                    "Permission $permissionKey ${if (add) "added to" else "removed from"} user ${user.mention}"
                )
                .block()
        }
    }

    private fun editGroupPermission(
        event: ChatInputInteractionEvent,
        add: Boolean
    ) {
        val guild = event.interaction.guild.block()!!
        val rootOption =
            event.getOption("group").get().getOption(if (add) "add" else "remove").get()
        val groupName = rootOption.getOption("group").get().value.get().asString()
        val permissionKey = rootOption.getOption("permission").get().value.get().asString()
        val permission: BasePermission? = PermissionRegistry.get(permissionKey)

        if (permission === null)
            event.interactionResponse.createFollowupMessage("Permission $permissionKey not found").block()
        else {
            val success: Boolean =
                if (add) PermissionManager.addPermission(guild, groupName, permission)
                else PermissionManager.removePermission(guild, groupName, permission)

            if (success)
                event.interactionResponse.createFollowupMessage(
                    "Permission $permissionKey ${if (add) "added to" else "removed from"} group $groupName"
                ).block()
            else
                event.interactionResponse.createFollowupMessage("Group $groupName not found").block()
        }

    }

    private fun getGroupList(event: ChatInputInteractionEvent) {
        val guild: Guild = event.interaction.guild.block()!!
        val groups: Collection<Group> = PermissionManager.getGroups(guild)

        buildString {
            appendLine("```Available groups for guild ${guild.name} :")
            appendLine()
            groups.forEach { appendLine(it.name) }
            append("```")
        }.also {
            event.interactionResponse.createFollowupMessage(it).block()
        }
    }

    private fun editGroup(event: ChatInputInteractionEvent, add: Boolean) {
        val guild = event.interaction.guild.block()!!
        val rootOption =
            event.getOption("group").get().getOption(if (add) "create" else "delete")
                .get()
        val groupName = rootOption.getOption("group").get().value.get().asString()

        if (add && PermissionManager.addGroup(guild, groupName) == null)
            event.interactionResponse.createFollowupMessage("Group $groupName already exist").block()
        else if (!add && !PermissionManager.removeGroup(guild, groupName))
            event.interactionResponse.createFollowupMessage("Group $groupName doesn't exist").block()
        else
            event.interactionResponse.createFollowupMessage("${if (add) "Added" else "Removed"} group $groupName")
                .block()
    }

    private fun setGroup(event: ChatInputInteractionEvent) {
        val guild = event.interaction.guild.block()!!
        val rootOption = event.getOption("user").get().getOption("setgroup").get()
        val user = rootOption.getOption("user").get().value.get().asUser().block()!!
        val groupName = rootOption.getOption("group").get().value.get().asString().lowercase()

        PermissionManager.setGroup(guild, user, groupName).let { success ->
            event.interactionResponse.createFollowupMessage(
                if (success)
                    "Set group $groupName to user ${user.mention}"
                else
                    "Group $groupName not found"
            ).block()
        }
    }

    override fun provideSlashCommand(): ApplicationCommandRequest? {
        return ApplicationCommandRequest.builder()
            .name("permissions")
            .description("Manage permissions")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("list")
                    .description("Get the list of available permissions")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("user")
                    .description("Manage user permissions")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND_GROUP.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("list")
                            .description("Get the list of permissions set on a user")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("user")
                                    .description("The user for which to get permissions")
                                    .type(ApplicationCommandOption.Type.USER.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("setgroup")
                            .description("Set a group on a user")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("user")
                                    .description("The user for which to set group")
                                    .type(ApplicationCommandOption.Type.USER.value)
                                    .required(true)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("The name of the group yo set")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("add")
                            .description("Add a permission to user")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("user")
                                    .description("Target user")
                                    .type(ApplicationCommandOption.Type.USER.value)
                                    .required(true)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("permission")
                                    .description("Permission to add")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("remove")
                            .description("Remove a permission from user")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("user")
                                    .description("Target user")
                                    .type(ApplicationCommandOption.Type.USER.value)
                                    .required(true)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("permission")
                                    .description("Permission to remove")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("group")
                    .description("Manage group permissions")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND_GROUP.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("list")
                            .description("Get the list of groups available or list of permissions set on a group")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("The name of the group for which to get permissions")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(false)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("create")
                            .description("Create a group")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("The name of the new group")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("delete")
                            .description("Delete a group")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("The name of the group to delete")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("add")
                            .description("Add a permission to group")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("Name of the group")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("permission")
                                    .description("Permission to add")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("remove")
                            .description("Remove a permission from a group")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("group")
                                    .description("Name of the group")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("permission")
                                    .description("Permission to remove")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(true)
                                    .build()
                            )
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("check")
                    .description("Check if a user has a permission")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("user")
                            .description("Target user")
                            .type(ApplicationCommandOption.Type.USER.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("permission")
                            .description("Permission to check")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .build()
    }
}