package com.omega.discord.bot.command.impl.music

import com.omega.discord.bot.audio.AudioPlayerManager
import com.omega.discord.bot.audio.GuildAudioPlayerManager
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel


class LeaveCommand : TextualCommand() {
    override val name: String = "leave"
    override val aliases: Array<String> = arrayOf("l")
    override val usage: String = "**leave** - Leave the voice channel"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_LEAVE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 5

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = (channel as GuildMessageChannel).guild.block()!!
        val playerManager: GuildAudioPlayerManager = AudioPlayerManager.getAudioManager(guild)
        val voiceConnection = playerManager.voiceConnection

        if (voiceConnection != null) {
            voiceConnection.disconnect().block()
            playerManager.voiceConnection = null
            MessageSender.sendMessage(channel, "Left voice channel")
        } else
            MessageSender.sendMessage(channel, "Not connected to a voice channel")
    }
}