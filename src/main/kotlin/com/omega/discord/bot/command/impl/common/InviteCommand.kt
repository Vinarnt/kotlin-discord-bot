package com.omega.discord.bot.command.impl.common

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel


class InviteCommand : TextualCommand() {
    override val name: String = "invite"
    override val aliases: Array<String>? = null
    override val usage: String = "**invite** - Send a DM with the link to invite the bot"
    override val allowPrivate: Boolean = true
    override val permission: Permission = Permission.COMMAND_INVITE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val clientId = BotManager.applicationInfo.id.asLong()
        val inviteLink = "https://discordapp.com/oauth2/authorize?client_id=$clientId&scope=bot%20applications.commands&permissions=2419141702"

        MessageSender.sendPrivateMessage(author, "Here the link to invite me to your servers : $inviteLink")
    }
}