package com.omega.discord.bot.command

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.exception.MissingArgumentException
import com.omega.discord.bot.exception.MissingPermissionException
import com.omega.discord.bot.ext.getNameAndDiscriminator
import com.omega.discord.bot.permission.BasePermission
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.discordjson.json.ApplicationCommandRequest
import org.slf4j.Logger
import org.slf4j.LoggerFactory


abstract class TextualCommand: Command {
    private val logger: Logger = LoggerFactory.getLogger("Command")

    abstract val aliases: Array<String>?
    abstract val usage: String
    abstract val allowPrivate: Boolean

    /**
     * Cooldown in seconds before next command for all users
     */
    abstract val globalCooldown: Long

    private var lastCommandTimestamp: Long = 0

    fun executeInternal(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val newCommandTimestamp = System.currentTimeMillis() / 1000

        // Execute only time since last command is higher than the global cooldown
        if (
            newCommandTimestamp - lastCommandTimestamp >= globalCooldown
            || BotManager.applicationInfo.owner.block() == author
            || (channel is GuildMessageChannel && author == channel.guild.block()?.owner?.block())
        ) {
            lastCommandTimestamp = newCommandTimestamp
            execute(author, channel, message, args)
        } else {
            message.addReaction(ReactionEmoji.unicode("⏱")).block()
        }
    }

    abstract fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>)

    protected fun missingArgs() {
        throw MissingArgumentException("One or more arguments are missing\n\n***Usage :***\n\n$usage")
    }

    protected fun missingPermission(message: Message, missingPermission: BasePermission) {
        throw MissingPermissionException(
            "User ${
                message.author.get().getNameAndDiscriminator()
            } doesn't have permission $missingPermission to run command $name"
        )
    }
}