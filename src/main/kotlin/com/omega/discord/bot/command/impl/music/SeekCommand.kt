package com.omega.discord.bot.command.impl.music

import com.omega.discord.bot.audio.AudioPlayerManager
import com.omega.discord.bot.audio.NotSeekableException
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.ext.seek
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.spec.EmbedCreateSpec
import java.util.function.Consumer


class SeekCommand : TextualCommand() {
    override val name: String = "seek"
    override val aliases: Array<String>? = null
    override val usage: String =
        "**seek <position>** - Seek to the given position in the playing track (Format: HH:mm:ss)"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_SEEK
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 15

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = (channel as GuildMessageChannel).guild.block()!!
        val audioManager = AudioPlayerManager.getAudioManager(guild)
        val playingTrack = audioManager.audioPlayer.playingTrack

        if (args.isNotEmpty()) {
            val durationStr = args.first()
            val skipTo = try {
                if (playingTrack == null) {
                    MessageSender.sendMessage(channel, "No track currently playing !")
                    null
                } else {
                    StringUtils.parseDuration(durationStr).coerceAtMost(playingTrack.duration)
                }
            } catch (e: NumberFormatException) {
                MessageSender.sendMessage(channel, "The duration must be in format HH:mm:ss")
                null
            }

            skipTo?.let {
                try {
                    val oldPosition = playingTrack.position
                    audioManager.audioPlayer.seek(it)
                    val position = playingTrack.position
                    val duration = playingTrack.duration
                    val fromStr = StringUtils.formatDuration(oldPosition)
                    val toStr = StringUtils.formatDuration(it)
                    val progressStr = "($toStr/${StringUtils.formatDuration(duration)}) " +
                            StringUtils.getTrackAsciiProgressBar(position, duration, 40)

                    MessageSender.sendMessage(
                        channel,
                        EmbedCreateSpec.builder().title("Seeking")
                            .addField("Track", playingTrack.info.title, false)
                            .addField("From", fromStr, true)
                            .addField("To", toStr, true)
                            .addField("Progress", progressStr, false)
                            .build()
                    )
                } catch (e: IllegalStateException) {
                    MessageSender.sendMessage(channel, "No track currently playing !")
                } catch (e: NotSeekableException) {
                    MessageSender.sendMessage(channel, "The playing track is not seekable !")
                }
            }
        } else
            missingArgs()
    }
}