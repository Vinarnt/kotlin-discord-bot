package com.omega.discord.bot.command.impl.config

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.RolePropertyValue
import com.omega.discord.bot.service.MessageSender
import discord4j.core.event.domain.guild.MemberJoinEvent
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.Role
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import reactor.core.publisher.Mono


class AutoRoleCommand : TextualCommand() {
    override val name: String = "autorole"
    override val aliases: Array<String> = arrayOf("ar")
    override val usage: String =
        "**autorole <roleMention>** - Automatically assign the mentioned role when a user join the server\n" +
                "**autorole <roleName>** - Automatically assign the named role when a user join the server\n" +
                "**autorole none** - To don't assign any role when users join the server"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_AUTOROLE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    init {
        BotManager.gateway.on(MemberJoinEvent::class.java) { event ->
            val autoroleProperty =
                GuildPropertyManager.get(event.guild.block()!!, GuildProperty.AUTOROLE) as RolePropertyValue
            autoroleProperty.value?.let { role: Role ->
                event.member.addRole(role.id, "Auto assigned on join by bot").doOnError { error ->
                    MessageSender.sendPrivateMessage(
                        event.guild.block()!!.owner.block() as User,
                        "${error.message}\nUnable to assign role automatically"
                    )
                }.block()
            }
            Mono.empty<MemberJoinEvent>()
        }.subscribe()
    }

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = (channel as GuildMessageChannel).guild.block()!!
        when {
            args.isEmpty() -> missingArgs()
            args.first().equals("none", true) -> { // none parameter
                if (GuildPropertyManager.set(guild, GuildProperty.AUTOROLE, RolePropertyValue()))
                    MessageSender.sendMessage(channel, "No role will be automatically assigned to new members")
                else
                    MessageSender.sendMessage(channel, "Unable to set autorole")
            }
            // It's a mentioned role
            StringUtils.isRoleMention(args.first()) -> {
                val role = StringUtils.parseRoleMention(guild, args.first())
                role?.let {
                    setAutorole(channel, role)
                }
            }
            // It's a role name
            else -> {
                val roleName = args.joinToString(" ")
                channel.guild.block()
                    ?.roles
                    ?.filter { role -> role.name.equals(roleName, true) }
                    ?.blockFirst()
                    ?.let { setAutorole(channel, it) }
                    ?: MessageSender.sendMessage(channel, "Unable to find role with name $roleName")
            }
        }
    }

    private fun setAutorole(channel: GuildMessageChannel, role: Role) {
        if (GuildPropertyManager.set(channel.guild.block()!!, GuildProperty.AUTOROLE, RolePropertyValue(role))) {
            MessageSender.sendMessage(
                channel,
                "New members will automatically be assigned to role ${role.name}\n" +
                        "The bot need the Manage Roles permission for it to work"
            )
        } else
            MessageSender.sendMessage(channel, "Unable to set autorole")
    }
}