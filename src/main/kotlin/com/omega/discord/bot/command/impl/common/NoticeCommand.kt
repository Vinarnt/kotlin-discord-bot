package com.omega.discord.bot.command.impl.common

import com.omega.discord.bot.command.SlashCommand
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.map.StringMapPropertyValue
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.User
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest


class NoticeCommand : SlashCommand() {
    override val name: String = "notice"
    override val permission: BasePermission = Permission.COMMAND_NOTICE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(event: ChatInputInteractionEvent) {
        when (val actionName = event.options.first().name) {
            // Get list of available notices
            "list" -> getNotices(event)
            // Add a notice
            "add" -> addNotice(event)
            // Remove a notice
            "remove" -> removeNotice(event)
            // Print the named notice
            "get" -> getNotice(event)
            // Unhandled
            else -> event.reply("action $actionName not found").withEphemeral(true).block()
        }
    }

    private fun checkPermission(guild: Guild, author: User): Boolean =
        if (PermissionManager.hasPermission(guild, author, Permission.COMMAND_NOTICE_ADMIN))
            true
        else {
            missingPermission(author, Permission.COMMAND_NOTICE_ADMIN)
            false
        }

    private fun getNotices(event: ChatInputInteractionEvent) {
        val availableNotices =
            GuildPropertyManager.get(event.interaction.guild.block()!!, GuildProperty.NOTICES).value as MutableMap<*, *>
        val builder = StringBuilder()

        if (availableNotices.isEmpty()) {
            builder.append("**No notices available**")
        } else {
            builder.append("**Available notices:**\n\n")
            val noticesStr = availableNotices
                .keys
                .joinToString("\n")
            builder.append(noticesStr)
        }

        event.reply(builder.toString()).withEphemeral(true).block()
    }

    private fun getNotice(event: ChatInputInteractionEvent) {
        val noticeName: String =
            event.getOption("get").get().getOption("name").get().value.get().asString()
        val notices =
            GuildPropertyManager.get(event.interaction.guild.block()!!, GuildProperty.NOTICES).value as MutableMap<*, *>

        val noticeContent = notices[noticeName]
        if (noticeContent !== null)
            event.reply(noticeContent as String).block()
        else
            event.reply("No notice found for $noticeName").withEphemeral(true).block()
    }

    private fun addNotice(event: ChatInputInteractionEvent) {
        val guild: Guild = event.interaction.guild.block()!!
        val author: User = event.interaction.member.get()

        if (checkPermission(guild, author)) {
            val actionOption = event.getOption("add").get()
            val noticeName: String = actionOption.getOption("name").get().value.get().asString()
            val noticeContent: String = actionOption.getOption("content").get().value.get().asString()
            val propertyValue = GuildPropertyManager.get(guild, GuildProperty.NOTICES) as StringMapPropertyValue
            val notices: MutableMap<String, String> = propertyValue.value
            notices[noticeName] = noticeContent
            GuildPropertyManager.set(guild, GuildProperty.NOTICES, propertyValue)

            event.reply("Added notice $noticeName").withEphemeral(true).block()
        }
    }

    private fun removeNotice(event: ChatInputInteractionEvent) {
        val guild: Guild = event.interaction.guild.block()!!
        val author: User = event.interaction.member.get()

        if (checkPermission(guild, author)) {
            val actionOption = event.getOption("remove").get()
            val noticeName: String = actionOption.getOption("name").get().value.get().asString()
            val propertyValue = GuildPropertyManager.get(guild, GuildProperty.NOTICES) as StringMapPropertyValue
            val notices: MutableMap<String, String> = propertyValue.value
            notices.remove(noticeName)
            GuildPropertyManager.set(guild, GuildProperty.NOTICES, propertyValue)

            event.reply("Removed notice $noticeName").withEphemeral(true).block()
        }
    }

    override fun provideSlashCommand(): ApplicationCommandRequest? {
        return ApplicationCommandRequest.builder()
            .name("notice")
            .description("Send or edit a predefined message")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("get")
                    .description("Send a notice")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("name")
                            .description("Notice name to send")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("list")
                    .description("Get a list of available notices")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("add")
                    .description("Add or replace a notice")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("name")
                            .description("Name of the notice to add or replace")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("content")
                            .description("Content of the notice")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("remove")
                    .description("Remove a notice")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("name")
                            .description("Name of the notice to remove")
                            .type(ApplicationCommandOption.Type.STRING.value)
                            .required(true)
                            .build()
                    )
                    .build()
            )
            .build()
    }
}