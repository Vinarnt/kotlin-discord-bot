package com.omega.discord.bot.command.impl.moderation

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.rest.http.client.ClientException

class BanCommand : TextualCommand() {
    override val name: String = "ban"
    override val aliases: Array<String>? = null
    override val usage: String = "**ban <userMention>** - Ban the mentioned user from the server" +
            "**ban <userMention> <reason>** - Ban the mentioned user from the server with a reason"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_BAN
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        if (args.isNotEmpty()) {
            val userMentionStr = args.first()
            val user: User? = StringUtils.parseUserMention(userMentionStr)

            if (user != null) {
                val reason: String? = if (args.size > 1) args.drop(1).joinToString(" ") else null

                try {
                    (channel as GuildMessageChannel).guild
                            .block()!!
                            .ban(user.id) { spec -> spec.reason = reason }
                            .doOnSuccess {
                                MessageSender.sendMessage(channel,
                                        "**Banned user ${user.mention}**" +
                                                if (reason != null) "\n**Reason:** $reason" else "")
                            }
                            .block()
                } catch (t: Throwable) {
                    when (t) {
                        is ClientException -> {
                            when (t.status.code()) {
                                403 -> MessageSender.sendMessage(channel, "Permission to ban needed")
                            }
                        }
                        else -> throw t
                    }
                }
            } else {
                MessageSender.sendMessage(channel, "User $userMentionStr not found")
            }
        } else {
            missingArgs()
        }
    }
}