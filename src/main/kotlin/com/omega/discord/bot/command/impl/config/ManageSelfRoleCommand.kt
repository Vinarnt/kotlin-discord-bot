package com.omega.discord.bot.command.impl.config

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.RoleSetPropertyValue
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.Role
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel


class ManageSelfRoleCommand : TextualCommand() {
    override val name: String = "manageSelfRole"
    override val aliases: Array<String> = arrayOf("msr")
    override val usage: String = "**manageSelfRole add <roleName>** - Add role to the list of self assignable roles\n" +
            "**manageSelfRole remove <roleName>** - Remove the role from the list of self assignable roles"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_MANAGE_SELF_ROLE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        if (args.size < 2) {
            missingArgs()
            return
        }

        val guild = (channel as GuildMessageChannel).guild.block()!!
        val roleName = args.drop(1).joinToString(" ")
        val result: List<Role> = guild.roles
                .filter { role -> role.name.equals(roleName, true) }
                .collectList()
                .block()!!
        when {
            result.isEmpty() -> MessageSender.sendMessage(channel, "Role $roleName not found")
            result.size > 1 -> MessageSender.sendMessage(channel, "Unable to get role $roleName, several roles were found")
            else -> {
                val role: Role = result.first()
                when (args[0]) {
                    "add" -> addRole(channel, role)
                    "remove" -> removeRole(channel, role)
                }
            }
        }
    }

    private fun addRole(channel: GuildMessageChannel, role: Role) {
        val guild: Guild = channel.guild.block()!!
        val propertyValue = GuildPropertyManager.get(guild, GuildProperty.AVAILABLE_SELFROLES) as RoleSetPropertyValue
        val availableSelfRoles = propertyValue.value

        availableSelfRoles += role

        GuildPropertyManager.set(guild, GuildProperty.AVAILABLE_SELFROLES, propertyValue)
        MessageSender.sendMessage(channel, "Added role ${role.name} to self role list")
    }

    private fun removeRole(channel: GuildMessageChannel, role: Role) {
        val guild: Guild = channel.guild.block()!!
        val propertyValue = GuildPropertyManager.get(guild, GuildProperty.AVAILABLE_SELFROLES) as RoleSetPropertyValue
        val availableSelfRoles = propertyValue.value

        availableSelfRoles -= role

        GuildPropertyManager.set(guild, GuildProperty.AVAILABLE_SELFROLES, propertyValue)
        MessageSender.sendMessage(channel, "Removed role ${role.name} from self role list")
    }
}