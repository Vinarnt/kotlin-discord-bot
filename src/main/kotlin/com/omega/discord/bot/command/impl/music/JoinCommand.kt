package com.omega.discord.bot.command.impl.music

import com.omega.discord.bot.audio.AudioPlayerManager
import com.omega.discord.bot.audio.GuildAudioPlayerManager
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.AudioChannel
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.rest.util.Permission as Discord4JPermission


class JoinCommand : TextualCommand() {
    override val name: String = "join"
    override val aliases: Array<String> = arrayOf("j")
    override val usage: String = "**join** - Join your current voice channel"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_JOIN
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 5

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = (channel as GuildMessageChannel).guild.block()!!
        val self: User = channel.client.self.block()!!
        val playerManager: GuildAudioPlayerManager = AudioPlayerManager.getAudioManager(guild)
        val voiceChannel: AudioChannel? = guild
            .voiceStates.filter { it.user.block() == author }.blockFirst()
            ?.channel!!.block()

        if (voiceChannel == null)
            MessageSender.sendMessage(channel, "You are not in a voice channel")

        voiceChannel?.let {
            when {
                !voiceChannel.getEffectivePermissions(channel.client.selfId).block()!!
                    .contains(Discord4JPermission.CONNECT) ->
                    MessageSender.sendMessage(
                        channel,
                        "Can't join the voice channel ${voiceChannel.name} : Missing permission CONNECT"
                    )

                voiceChannel.voiceStates.filter { it.user.block() == self }.blockFirst() != null ->
                    MessageSender.sendMessage(channel, "Already connected to this channel")

                else -> {
                    voiceChannel
                        .join { spec ->
                            spec.setProvider(AudioPlayerManager.getAudioManager(guild).getAudioProvider())
                        }
                        .block()
                        .also { playerManager.voiceConnection = it }
                    MessageSender.sendMessage(channel, "Joined channel **${voiceChannel.name}**")
                }
            }
        }
    }
}