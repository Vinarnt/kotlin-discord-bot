package com.omega.discord.bot.command

import com.omega.discord.bot.exception.MissingPermissionException
import com.omega.discord.bot.ext.getNameAndDiscriminator
import com.omega.discord.bot.permission.BasePermission
import discord4j.core.`object`.entity.User
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandRequest


abstract class SlashCommand : Command {
    /**
     * Cooldown in seconds before next command for all users
     */
    abstract val globalCooldown: Long

    fun executeInternal(event: ChatInputInteractionEvent) = execute(event)

    abstract fun execute(event: ChatInputInteractionEvent)

    abstract fun provideSlashCommand(): ApplicationCommandRequest?

    protected fun missingPermission(user: User, missingPermission: BasePermission) {
        throw MissingPermissionException(
            "User ${
                user.getNameAndDiscriminator()
            } doesn't have permission $missingPermission to run command $name"
        )
    }
}