package com.omega.discord.bot.command

import com.omega.discord.bot.command.impl.common.*
import com.omega.discord.bot.command.impl.config.AgreementCommand
import com.omega.discord.bot.command.impl.config.AutoRoleCommand
import com.omega.discord.bot.command.impl.config.ManageSelfRoleCommand
import com.omega.discord.bot.command.impl.config.PrefixCommand
import com.omega.discord.bot.command.impl.moderation.BanCommand
import com.omega.discord.bot.command.impl.moderation.KickCommand
import com.omega.discord.bot.command.impl.moderation.PurgeCommand
import com.omega.discord.bot.command.impl.music.*
import com.omega.discord.bot.command.impl.nsfw.NSFWCommand
import com.omega.discord.bot.command.impl.permission.PermissionsCommand


object CommandRegistry {

    private val commands: MutableMap<String, Command> = hashMapOf()
    private val aliases: MutableMap<String, Command> = hashMapOf()

    init {
        register(AgreementCommand())
        register(AutoRoleCommand())
        register(BanCommand())
        register(HelpCommand())
        register(InviteCommand())
        register(JoinCommand())
        register(KickCommand())
        register(LeaveCommand())
        register(ManageSelfRoleCommand())
        register(NoticeCommand())
        register(NSFWCommand())
        register(PermissionsCommand())
        register(PrefixCommand())
        register(PurgeCommand())
        register(QueueCommand())
        register(RollCommand())
        register(SayCommand())
        register(SeekCommand())
        register(SelfRoleCommand())
        register(SkipCommand())
    }

    fun register(command: Command) {
        commands.putIfAbsent(command.name, command)

        if (command is TextualCommand)
            command.aliases?.forEach { aliases.putIfAbsent(it, command) }
    }

    fun unregister(command: Command) {
        commands.remove(command.name)

        if (command is TextualCommand)
            command.aliases?.forEach { aliases.remove(it) }
    }

    fun get(commandName: String): Command? = commands[commandName] ?: aliases[commandName]

    fun get(): Iterable<Command> = commands.values.toList()
}