package com.omega.discord.bot.command.impl.nsfw

import com.omega.discord.bot.command.SlashCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.nsfw.BoobAndButtService
import discord4j.core.`object`.command.ApplicationCommandOption
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.entity.channel.TextChannel
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import discord4j.discordjson.json.ApplicationCommandOptionChoiceData
import discord4j.discordjson.json.ApplicationCommandOptionData
import discord4j.discordjson.json.ApplicationCommandRequest
import net.kodehawa.lib.imageboards.DefaultImageBoards
import net.kodehawa.lib.imageboards.ImageBoard
import net.kodehawa.lib.imageboards.entities.Rating
import net.kodehawa.lib.imageboards.entities.impl.KonachanImage
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.IOException
import java.net.SocketTimeoutException
import kotlin.random.Random
import kotlin.random.nextInt


class NSFWCommand : SlashCommand() {
    private val logger: Logger = LoggerFactory.getLogger("NSFWCommand")

    override val name: String = "nsfw"
    override val permission: Permission = Permission.COMMAND_NSFW
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 1

    private val HENTAI_TAGS_BLACKLIST = arrayOf("loli", "lolicon", "shota", "shotacon")
    private val HENTAI_LIMIT_PER_PAGE = 100
    private val HENTAI_MAX_PAGE = 280
    private val MAX_RETRY = 3

    init {
        ImageBoard.setUserAgent("Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:90.0) Gecko/20100101 Firefox/90.0")
    }

    override fun execute(event: ChatInputInteractionEvent) {
        val channel: MessageChannel = event.interaction.channel.block()!!

        if (channel is GuildMessageChannel && !(channel as TextChannel).isNsfw) {
            event.reply("Command unavailable outside of an NSFW channel").withEphemeral(true)
                .block()
        } else {
            when (val actionName = event.options.first().name) {
                "get" -> {
                    when (event.getOption("get").get().options.first().name) {
                        "hentai" -> sendHentai(event)
                        "boobs" -> sendBoobOrButt(event, BoobAndButtService.ImageType.BOOBS)
                        "butts" -> sendBoobOrButt(event, BoobAndButtService.ImageType.BUTTS)
                    }
                }
                else -> event.interactionResponse.createFollowupMessage("Category $actionName not found").block()
            }
        }
    }

    private fun sendHentai(event: ChatInputInteractionEvent) {
        val rootOption = event.getOption("get").get().options.first()
        val count = rootOption.getOption("count")
            .orElse(null)?.value?.get()?.raw?.toInt()
            ?: 5
        val private = rootOption.getOption("private")
            .orElse(null)?.value?.get()?.asBoolean()
            ?: false
        val tags = rootOption.getOption("tags")
            .orElse(null)?.value?.get()?.asString()
        val rating = rootOption.getOption("rating")
            .orElse(null)?.value?.get()?.asString()?.toInt()?.let { Rating.values()[it] }
            ?: if (Random.nextBoolean()) Rating.QUESTIONABLE else Rating.EXPLICIT
        val page = getRandomPage()

        event.let { if (private) it.deferReply().withEphemeral(true) else it.deferReply() }
            .doOnSuccess {
                getHentaiPictures(page, rating, tags).let { pictures ->
                    val picturesStore = mutableListOf<KonachanImage>().apply {
                        addAll(pictures)
                    }

                    // Fetch pictures until the count is reached
                    var retry = 0
                    while (tags === null && picturesStore.size < count && retry < MAX_RETRY) {
                        Thread.sleep(500L)
                        retry++
                        picturesStore.addAll(getHentaiPictures(getRandomPage(), rating, null))
                    }

                    if (picturesStore.isEmpty()) {
                        event.interactionResponse.createFollowupMessage("No pictures found").block()
                    } else {
                        buildString {
                            picturesStore
                                .shuffled()
                                .take(count)
                                .forEach { picture ->
                                    appendLine(picture.file_url)
                                    appendLine()
                                }
                        }.let {
                            event.interactionResponse.createFollowupMessage(it).block()
                        }
                    }
                }
            }.block()
    }

    private fun getHentaiPictures(page: Int, rating: Rating, tags: String?, retry: Int = 0): List<KonachanImage> {
        return try {
            logger.info(
                buildString {
                    append("Get ")
                    append(rating.longName)
                    append(" hentai pictures")

                    if (tags.isNullOrEmpty()) {
                        append(" from page ")
                        append(page)
                        append(" of ")
                        append(HENTAI_MAX_PAGE)
                    } else {
                        appendLine(" with tags:")
                        append(tags)
                    }
                }
            )
            DefaultImageBoards.KONACHAN.let {
                if (tags === null) it.get(page, HENTAI_LIMIT_PER_PAGE, rating) else it.search(
                    1,
                    1000,
                    tags,
                    rating
                )
            }
                .blocking()
                .filter {
                    !it.isPending && !it.tags.any(HENTAI_TAGS_BLACKLIST::contains)
                }
//                .onEach { konachanImage -> logger.info(konachanImage.tags.joinToString(" ")) }
                .also { logger.info("Got ${it.size} pictures") }
        } catch (e: SocketTimeoutException) {
            if (retry < MAX_RETRY) {
                Thread.sleep(1000L)
                return getHentaiPictures(page, rating, tags, retry + 1)
            }

            return emptyList()
        } catch (e: Exception) {
            this.logger.error("Unable to query for pictures", e)
            return emptyList()
        }
    }

    private fun getRandomPage(): Int = Random.nextInt(1..HENTAI_MAX_PAGE)

    private fun sendBoobOrButt(
        event: ChatInputInteractionEvent,
        imageType: BoobAndButtService.ImageType
    ) {
        val rootOption = event.getOption("get").get().options.first()
        val count = rootOption.getOption("count")
            .orElse(null)?.value?.get()?.raw?.toInt()
            ?: 5
        val private = rootOption.getOption("private")
            .orElse(null)?.value?.get()?.asBoolean()
            ?: false

        event.let { if (private) it.deferReply().withEphemeral(true) else it.deferReply() }
            .doOnSuccess {
                BoobAndButtService.getRandomImages(imageType, count, object : BoobAndButtService.ResultCallback {
                    override fun onFailure(e: IOException) {
                        event.interactionResponse.createFollowupMessage("Request failed").block()
                    }

                    override fun onResult(results: List<BoobAndButtService.NSFWResult>) {
                        val stringBuilder = buildString {
                            results.forEachIndexed { i, result ->
                                result.modelName?.let { append("Model: ").append(it).append('\n') }
                                append(result.imageUrl)

                                if (i < results.size - 1)
                                    append("\n\n")
                            }
                        }

                        event.interactionResponse.createFollowupMessage(stringBuilder).block()
                    }
                })
            }.block()
    }

    override fun provideSlashCommand(): ApplicationCommandRequest? {
        val countOption = ApplicationCommandOptionData.builder()
            .name("count")
            .description("Number of pictures to get (Limited to 5)")
            .type(ApplicationCommandOption.Type.STRING.value)
            .choices(
                listOf(
                    ApplicationCommandOptionChoiceData.builder().name("1").value("1").build(),
                    ApplicationCommandOptionChoiceData.builder().name("2").value("2").build(),
                    ApplicationCommandOptionChoiceData.builder().name("3").value("3").build(),
                    ApplicationCommandOptionChoiceData.builder().name("4").value("4").build(),
                    ApplicationCommandOptionChoiceData.builder().name("5").value("5").build()
                )
            )
            .required(false)
            .build()
        val privateOption = ApplicationCommandOptionData.builder()
            .name("private")
            .description("Hide the command from others")
            .type(ApplicationCommandOption.Type.BOOLEAN.value)
            .required(false)
            .build()

        return ApplicationCommandRequest.builder()
            .name("nsfw")
            .description("Get nsfw pictures")
            .addOption(
                ApplicationCommandOptionData.builder()
                    .name("get")
                    .description("Get nsfw pictures")
                    .type(ApplicationCommandOption.Type.SUB_COMMAND_GROUP.value)
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("boobs")
                            .description("Get nsfw pictures of boobs")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(countOption)
                            .addOption(privateOption)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("butts")
                            .description("Get nsfw pictures of butts")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(countOption)
                            .addOption(privateOption)
                            .build()
                    )
                    .addOption(
                        ApplicationCommandOptionData.builder()
                            .name("hentai")
                            .description("Get hentai pictures")
                            .type(ApplicationCommandOption.Type.SUB_COMMAND.value)
                            .addOption(countOption)
                            .addOption(privateOption)
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("rating")
                                    .description("Softness rating of the pictures")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .addAllChoices(
                                        listOf(
                                            ApplicationCommandOptionChoiceData.builder()
                                                .name("QUESTIONABLE")
                                                .value("1")
                                                .build(),
                                            ApplicationCommandOptionChoiceData.builder()
                                                .name("EXPLICIT")
                                                .value("2")
                                                .build()
                                        )
                                    )
                                    .required(false)
                                    .build()
                            )
                            .addOption(
                                ApplicationCommandOptionData.builder()
                                    .name("tags")
                                    .description("Tags to search for (eg. animal_ears yuri)")
                                    .type(ApplicationCommandOption.Type.STRING.value)
                                    .required(false)
                                    .build()
                            )
                            .build()
                    )
                    .build()
            )
            .build()
    }
}