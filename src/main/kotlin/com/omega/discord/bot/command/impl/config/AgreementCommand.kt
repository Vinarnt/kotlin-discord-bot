package com.omega.discord.bot.command.impl.config

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.MessagePropertyValue
import com.omega.discord.bot.property.type.RolePropertyValue
import com.omega.discord.bot.service.AgreementService
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.reaction.ReactionEmoji


class AgreementCommand : TextualCommand() {
    override val name: String = "agreement"
    override val aliases: Array<String> = arrayOf("agr")
    override val usage: String = "**agreement message <message>** - Send the message in the current channel with a reaction added to it\n" +
            "**agreement role <roleName>** - Set the role to assign when the message reaction is clicked"
    override val allowPrivate: Boolean = false
    override val permission: BasePermission = Permission.COMMAND_AGREEMENT
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = message.guild.block()!!
        when {
            // Missing arguments
            args.isEmpty() -> missingArgs()
            // Set message
            args.first().equals("message", true) -> {
                if (args.size < 2)
                    missingArgs()

                val oldMessage = GuildPropertyManager.get(guild, GuildProperty.AGREEMENT_MESSAGE).value as Message?
                oldMessage?.delete()?.block()

                MessageSender.sendMessage(channel, args.drop(1).joinToString(" ")).also {
                    GuildPropertyManager.set(guild, GuildProperty.AGREEMENT_MESSAGE, MessagePropertyValue(it))
                    it.addReaction(ReactionEmoji.unicode(AgreementService.reaction)).block()
                    AgreementService.addSubscribers(guild)
                }
            }
            // Set role
            args.first().equals("role", true) -> {
                val roleName = args.drop(1).joinToString(" ")
                val role = guild.roles.filter { it.name.equals(roleName, true) }.blockFirst()
                if (role != null) {
                    GuildPropertyManager.set(guild, GuildProperty.AGREEMENT_ROLE, RolePropertyValue(role))
                    MessageSender.sendMessage(channel, "Agreement role set to ${role.name}")
                } else {
                    MessageSender.sendMessage(channel, "Role $roleName not found")
                }
            }
            else -> missingArgs()
        }
    }
}