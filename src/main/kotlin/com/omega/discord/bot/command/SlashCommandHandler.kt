package com.omega.discord.bot.command

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.exception.MissingPermissionException
import com.omega.discord.bot.ext.getNameAndDiscriminator
import com.omega.discord.bot.permission.BasePermission
import com.omega.discord.bot.permission.PermissionManager
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.entity.channel.PrivateChannel
import discord4j.core.event.domain.interaction.ChatInputInteractionEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object SlashCommandHandler {
    private val logger: Logger = LoggerFactory.getLogger(SlashCommandHandler.javaClass)

    fun handle(event: ChatInputInteractionEvent) {
        val commandName = event.commandName
        val channel: MessageChannel = event.interaction.channel.block()!!
        val author: User = event.interaction.user
        val command: SlashCommand? = CommandRegistry.get(commandName) as SlashCommand?

        if (command == null) {
            logger.info("Command $commandName not found")
            event.reply("Command $commandName not found").withEphemeral(true).block()
        } else {
            val permission: BasePermission? = command.permission
            if (
                channel is PrivateChannel
                || (command.ownerOnly && author == BotManager.applicationInfo.owner.block())
                || permission == null && !command.ownerOnly
                || permission != null && PermissionManager.hasPermission(
                    event.interaction.guild.block()!!,
                    author,
                    permission
                )
            ) {
                try {
                    command.executeInternal(event)
                } catch (e: MissingPermissionException) {
                    logger.info(e.message)
                    event.reply("You do not have permission to use this command").withEphemeral(true).block()
                } catch (e: Exception) {
                    logger.warn("Command execution failed", e)
                    event.reply("Unhandled error occurred").withEphemeral(true).block()
                }
            } else {// No permission
                logger.info("User ${author.getNameAndDiscriminator()} doesn't have permission ${command.permission} to run command $commandName")
                event.reply("You do not have permission to use this command").withEphemeral(true).block()
            }
        }
    }
}