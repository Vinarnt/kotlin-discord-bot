package com.omega.discord.bot.command.impl.common

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.RoleSetPropertyValue
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Guild
import discord4j.core.`object`.entity.Member
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.Role
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.rest.http.client.ClientException


class SelfRoleCommand : TextualCommand() {
    override val name: String = "selfrole"
    override val aliases: Array<String> = arrayOf("sr")
    override val usage: String = "**selfrole list** - Get the list of available roles\n" +
            "**selfrole list <role_name>** - Get the users having the provided role" +
            "**selfrole add <role_name>** - Add the role to yourself\n" +
            "**selfrole remove <role_name>** - Remove the role from yourself"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_SELF_ROLE
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        if (args.isEmpty()) {
            missingArgs()
            return
        }

        val guild = (channel as GuildMessageChannel).guild.block()!!
        when (args[0]) {
            "list" -> {
                // Get the list of users having this role
                if (args.size > 1) {
                    val roleName = args.drop(1).joinToString(" ")
                    val role: Role? = guild.roles.filter { it.name.equals(roleName, true) }.blockFirst()
                    role?.also {
                        listUsers(channel, it)
                    } ?: MessageSender.sendMessage(channel, "Role $roleName not found")
                }
                // Get the list of roles available for self assigning
                else
                    listRoles(channel)
            }

            else -> {
                if (args.size < 2) {
                    missingArgs()
                    return
                }

                val roleName = args.drop(1).joinToString(" ")
                val result: List<Role> =
                    guild.roles.filter { role -> role.name.equals(roleName, true) }.collectList().block()!!
                when {
                    result.isEmpty() -> MessageSender.sendMessage(channel, "Role $roleName not found")
                    result.size > 1 -> MessageSender.sendMessage(
                        channel,
                        "Unable to get role $roleName, several roles were found"
                    )

                    else -> {
                        val propertyValue =
                            GuildPropertyManager.get(guild, GuildProperty.AVAILABLE_SELFROLES) as RoleSetPropertyValue
                        val availableRoles = propertyValue.value
                        val role: Role = result.first()

                        if (!availableRoles.contains(role)) {
                            MessageSender.sendMessage(channel, "Role ${role.name} is not a self assignable role")
                        } else {
                            when (args[0]) {
                                "add" -> addRole(channel, role, author)
                                "remove" -> removeRole(channel, role, author)
                            }
                        }
                    }
                }
            }
        }
    }

    private fun listRoles(channel: GuildMessageChannel) {
        val guild: Guild = channel.guild.block()!!
        val propertyValue = GuildPropertyManager.get(guild, GuildProperty.AVAILABLE_SELFROLES) as RoleSetPropertyValue
        val availableRoles: MutableSet<Role> = propertyValue.value
        val builder = StringBuilder()

        if (availableRoles.isEmpty()) {
            builder.append("**No role available**")
        } else {
            builder.append("**Available roles:**\n\n")
            val rolesStr = availableRoles
                .map { it.name }
                .toList()
                .joinToString("\n")
            builder.append(rolesStr)
        }
        MessageSender.sendMessage(channel, builder.toString())
    }

    private fun listUsers(channel: GuildMessageChannel, role: Role) {
        val members: List<Member> = channel.guild.block()!!.members.collectList().block()!!
        val users: List<Member> = members.filter { member ->
            member.roles.filter { _role -> _role == role }
                .hasElements().block()!!
        }
        val builder = StringBuilder()

        if (users.isEmpty()) {
            builder.append("**No users found**")
        } else {
            builder.append("**Users for role ${role.name}:**\n\n")
            val userStr = users
                .map { it.username }
                .toList()
                .joinToString("\n")
            builder.append(userStr)
        }
        MessageSender.sendMessage(channel, builder.toString())
    }

    private fun addRole(channel: GuildMessageChannel, role: Role, user: User) {
        try {
            user.asMember(channel.guildId).block()!!
                .addRole(role.id)
                .doOnSuccess {
                    MessageSender.sendMessage(channel, "Role ${role.name} added")
                }
                .block()
        } catch (t: Throwable) {
            handleError(channel, t)
        }
    }

    private fun removeRole(channel: GuildMessageChannel, role: Role, user: User) {
        try {
            user.asMember(channel.guildId).block()!!
                .removeRole(role.id)
                .doOnSuccess {
                    MessageSender.sendMessage(channel, "Role ${role.name} removed")
                }
                .block()
        } catch (t: Throwable) {
            handleError(channel, t)
        }
    }

    private fun handleError(channel: MessageChannel, t: Throwable) {
        when (t) {
            is ClientException -> {
                when (t.status.code()) {
                    403 -> MessageSender.sendMessage(channel, "Permission to manage roles needed")
                }
            }

            else -> throw t
        }
    }
}