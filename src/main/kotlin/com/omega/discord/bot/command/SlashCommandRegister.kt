package com.omega.discord.bot.command

import com.omega.discord.bot.BotManager
import discord4j.core.`object`.entity.Guild
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import reactor.core.publisher.Mono


object SlashCommandRegister {
    private val logger: Logger = LoggerFactory.getLogger(this.javaClass)

    fun registerCommandsForGuild(guild: Guild) {
        val appId = guild.client.applicationInfo.block()!!.id.asLong()
        val guildId = guild.id.asLong()

        CommandRegistry.get().filterIsInstance<SlashCommand>().mapNotNull { it.provideSlashCommand() }
            .also {
                BotManager.client.applicationService.bulkOverwriteGuildApplicationCommand(appId, guildId, it)
                    .doOnNext { commandData ->
                        logger.info("Registered application command ${commandData.name()} for guild ${guild.name} (${guild.id.asLong()})")
                    }
                    .doOnError { e ->
                        logger.warn(
                            "Unable to create guild commands for guild ${guild.name} (${guild.id.asLong()})",
                            e
                        )
                    }
                    .onErrorResume { _ -> Mono.empty() }
                    .subscribe()
            }
    }
}