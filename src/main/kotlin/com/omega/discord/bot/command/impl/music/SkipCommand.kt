package com.omega.discord.bot.command.impl.music

import com.omega.discord.bot.audio.AudioPlayerManager
import com.omega.discord.bot.audio.TrackUserData
import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.permission.PermissionManager
import com.omega.discord.bot.service.MessageSender
import com.sedmelluq.discord.lavaplayer.track.AudioTrack
import discord4j.core.`object`.entity.*
import discord4j.core.`object`.entity.channel.AudioChannel
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.`object`.entity.channel.VoiceChannel
import discord4j.core.`object`.reaction.ReactionEmoji
import kotlin.math.ceil


class SkipCommand : TextualCommand() {
    override val name: String = "skip"
    override val aliases: Array<String>? = null
    override val usage: String = "**skip** - Skip the playing track\n" +
            "**skip force** - Skip the playing track without voting **(Need to be server owner or have permission command.skip.force)**\n" +
            "**skip <count>** - Skip nth tracks **(Need to be server owner or have permission command.skip.multiple)**"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_SKIP
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        val guild = (channel as GuildMessageChannel).guild.block()!!
        val self: User = guild.client.self.block()!!
        val playerManager = AudioPlayerManager.getAudioManager(guild)
        val botVoiceChanel: AudioChannel? = guild
                .voiceStates
                .collectList()
                .block()
                ?.firstOrNull { it.user.block() == self }
                ?.channel
                ?.block()
        val authorVoiceChannel: AudioChannel? = guild
                .voiceStates
                .collectList()
                .block()
                ?.firstOrNull { it.user.block() == author }
                ?.channel
                ?.block()
        val playingTrack: AudioTrack? = playerManager.audioPlayer.playingTrack

        if (botVoiceChanel == null)
            MessageSender.sendMessage(channel, "Bot not connected to a voice channel")
        else if (authorVoiceChannel == null)
            MessageSender.sendMessage(channel, "You are not connected to a voice channel")
        else if (botVoiceChanel != authorVoiceChannel)
            MessageSender.sendMessage(channel, "You need to be in the same voice channel as the bot")
        else {
            val forceSkip: Boolean = args.firstOrNull()?.equals("force", true) ?: false
            if (args.isEmpty()) { // Vote
                if (playingTrack == null) {
                    MessageSender.sendMessage(channel, "Not playing anything")
                    return
                }

                val trackUserData = playingTrack.userData as TrackUserData
                val votes: MutableSet<User> = trackUserData.skipVotes
                votes.add(author)

                val neededCount: Int = ceil(botVoiceChanel.voiceStates.count().block()!! * 0.5).toInt()
                val currentVotes: Int = votes.size

                if (currentVotes >= neededCount) {
                    playerManager.scheduler.skip(1)
                    MessageSender.sendMessage(channel, "Skipped track ${playingTrack.info.title}")
                } else
                    MessageSender.sendMessage(channel, "Vote to skip : $currentVotes / $neededCount")
            } else if (forceSkip) { // Force
                if (
                        author == message.guild.block()?.owner?.block()
                        || PermissionManager.hasPermission(guild, author, Permission.COMMAND_SKIP_FORCE)
                ) {
                    if (playingTrack == null) {
                        MessageSender.sendMessage(channel, "Not playing anything")
                        return
                    }
                    playerManager.scheduler.skip(1)
                    MessageSender.sendMessage(channel, "Skipped track ${playingTrack.info.title} (forced)")
                } else {
                    message.addReaction(ReactionEmoji.unicode("⛔")).block()
                }
            } else { // Multiple skip
                if (
                        author == message.guild.block()?.owner?.block()
                        || PermissionManager.hasPermission(guild, author, Permission.COMMAND_SKIP_MULTIPLE)) {
                    if (playingTrack == null) {
                        MessageSender.sendMessage(channel, "Not playing anything")
                        return
                    }

                    try {
                        val skipCount = args[0].toInt()
                        val skippedCount = playerManager.scheduler.skip(skipCount)
                        MessageSender.sendMessage(channel, "Skipped $skippedCount tracks")
                    } catch (e: NumberFormatException) {
                        MessageSender.sendMessage(channel, "The count parameter '${args[0]}' is not a number")
                    }
                } else {
                    message.addReaction(ReactionEmoji.unicode("⛔")).block()
                }
            }
        }
    }
}