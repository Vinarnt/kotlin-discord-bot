package com.omega.discord.bot.command

import com.omega.discord.bot.permission.BasePermission


interface Command {
    val name: String
    val permission: BasePermission?
    val ownerOnly: Boolean
}