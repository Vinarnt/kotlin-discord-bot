package com.omega.discord.bot.command.impl.common

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.ext.StringUtils
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel


class SayCommand : TextualCommand() {
    override val name: String = "say"
    override val aliases: Array<String>? = null
    override val usage: String = "**say <message>** - Make the bot say the message on the current channel\n" +
            "**say <chanelMention> <message>** - Make the bot say the message on the defined channel"
    override val allowPrivate: Boolean = false
    override val permission: Permission? = null
    override val ownerOnly: Boolean = true
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        when {
            // Missing arguments
            args.isEmpty() -> missingArgs()
            // Channel mention provided
            StringUtils.isChannelMention(args.first()) -> {
                if(args.size > 1) {
                    val targetChannel: MessageChannel? = StringUtils.parseChannelMention(args.first())
                    targetChannel?.let {
                        MessageSender.sendMessage(it, args.drop(1).joinToString(" "))
                    } ?: MessageSender.sendPrivateMessage(author, "Channel not found")
                } else {
                    missingArgs()
                }
            }
            // Send on current channel
            else -> MessageSender.sendMessage(channel, args.joinToString(" "))
        }
    }
}