package com.omega.discord.bot.command.impl.config

import com.omega.discord.bot.command.TextualCommand
import com.omega.discord.bot.permission.Permission
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import com.omega.discord.bot.property.type.StringPropertyValue
import com.omega.discord.bot.service.MessageSender
import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.GuildMessageChannel
import discord4j.core.`object`.entity.channel.MessageChannel


class PrefixCommand : TextualCommand() {
    override val name: String = "prefix"
    override val aliases: Array<String>? = null
    override val usage: String = "**prefix <prefix>** - Set the bot command prefix"
    override val allowPrivate: Boolean = false
    override val permission: Permission = Permission.COMMAND_PREFIX
    override val ownerOnly: Boolean = false
    override val globalCooldown: Long = 0

    override fun execute(author: User, channel: MessageChannel, message: Message, args: List<String>) {
        if(args.isEmpty()) {
            missingArgs()
        } else {
            val prefix = args.first()
            if(GuildPropertyManager.set((channel as GuildMessageChannel).guild.block()!!, GuildProperty.COMMAND_PREFIX, StringPropertyValue(prefix)))
                MessageSender.sendMessage(channel, "Bot prefix set to $prefix")
            else
                MessageSender.sendMessage(channel, "Unable to set prefix")
        }
    }
}