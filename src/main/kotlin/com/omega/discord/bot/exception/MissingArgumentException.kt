package com.omega.discord.bot.exception


class MissingArgumentException(message: String) : Exception(message)