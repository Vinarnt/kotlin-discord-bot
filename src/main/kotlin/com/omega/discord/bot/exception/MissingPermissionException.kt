package com.omega.discord.bot.exception


class MissingPermissionException(message: String) : Exception(message)