package com.omega.discord.bot.database.impl

import com.mongodb.DBRef
import com.mongodb.client.MongoDatabase
import com.omega.discord.bot.database.UserDAO
import com.omega.discord.bot.permission.Group
import com.omega.discord.bot.permission.PermissionOverride
import com.omega.discord.bot.permission.User
import discord4j.core.`object`.entity.Guild
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.litote.kmongo.util.KMongoUtil
import org.litote.kmongo.util.idValue
import kotlin.collections.toList


class MorphiaUserDAO(private val database: MongoDatabase) : UserDAO {
    override fun find(id: ObjectId): User? =
            database.getCollection<User>().findOneById(id)

    override fun findFor(guild: Guild): List<User> =
            database.getCollection<User>().find("{guild: ${guild.id.asLong()}}").toList()

    override fun insert(entity: User): User =
            entity.also { database.getCollection<User>().insertOne(entity) }

    override fun update(entity: User): User =
            entity.also { database.getCollection<User>().updateOne(entity) }

    override fun updatePermission(entity: User, permission: String, override: PermissionOverride) {
        val filterStr = "{_id: ObjectId(\"${entity.idValue}\")}"
        val updateStr = "{\$set: {\"permissions.${permission.replace(".", "_")}\": \"$override\"}}"

        database.getCollection<User>().updateOne(filterStr, updateStr)
    }

    override fun updateGroup(entity: User) {
        val filterStr = "{_id: ObjectId(\"${entity.idValue}\")}"
        val updateStr = "{\$set: { group: ${DBRef(KMongoUtil.defaultCollectionName(Group::class), entity.group.idValue!!)} } }"

        database.getCollection<User>()
                .updateOne(filterStr, updateStr)
    }

    override fun delete(entity: User) {
        database.getCollection<User>()
                .deleteOneById(entity.id!!)
    }

    override fun clean() {
    }
}