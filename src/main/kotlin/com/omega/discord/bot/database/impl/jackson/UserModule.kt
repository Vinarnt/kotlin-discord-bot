package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.omega.discord.bot.getUserFromId
import discord4j.core.`object`.entity.User


class UserModule : SimpleModule() {
    init {
        addSerializer(User::class.java, UserSerializer)
        addDeserializer(User::class.java, UserDeserializer)
    }

    private object UserDeserializer : JsonDeserializer<User>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): User? =
            p.readValueAs(Long::class.java)?.let { getUserFromId(it) }
    }

    private object UserSerializer : JsonSerializer<User>() {
        override fun serialize(value: User?, gen: JsonGenerator, serializers: SerializerProvider) {
            value?.let { gen.writeNumber(it.id.asLong()) } ?: gen.writeNull()
        }
    }
}