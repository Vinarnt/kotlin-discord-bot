package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import org.litote.kmongo.util.KMongoUtil
import kotlin.reflect.full.memberProperties


object DBReferenceSerializer : JsonSerializer<Any?>() {
    override fun serialize(value: Any?, gen: JsonGenerator, serializers: SerializerProvider) {
        if (value == null) gen.writeNull()
        else {
            // Get the object collection name and id
            val objectId = value::class.memberProperties.find { it.name == "id" }?.getter?.call(value)
            val collectionName = KMongoUtil.defaultCollectionName(value::class)

            gen.apply {
                writeStartObject()
                writeStringField("\$ref", collectionName)
                writeFieldName("\$id")
                writeObjectId(objectId)
                writeEndObject()
            }
        }
    }
}