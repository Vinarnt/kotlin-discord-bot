package com.omega.discord.bot.database

import com.omega.discord.bot.permission.Group
import discord4j.core.`object`.entity.Guild


interface GroupDAO : DAO<Group> {

    fun findFor(guild: Guild): List<Group>

    fun addPermission(entity: Group, permission: String)

    fun removePermission(entity: Group, permission: String)
}