package com.omega.discord.bot.database

import com.omega.discord.bot.permission.PermissionOverride
import com.omega.discord.bot.permission.User
import discord4j.core.`object`.entity.Guild


interface UserDAO : DAO<User> {

    fun findFor(guild: Guild): List<User>

    fun updatePermission(entity: User, permission: String, override: PermissionOverride)

    fun updateGroup(entity: User)
}