package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.omega.discord.bot.getMessageFromId
import discord4j.core.`object`.entity.Message


class MessageModule : SimpleModule() {
    init {
        addSerializer(Message::class.java, MessageSerializer)
        addDeserializer(Message::class.java, MessageDeserializer)
    }

    private object MessageDeserializer : JsonDeserializer<Message>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Message? =
                with(p) {
                    nextToken()
                    nextToken()
                    val channelId = readValueAs(Long::class.java)
                    nextToken()
                    nextToken()
                    val messageId = readValueAs(Long::class.java)
                    getMessageFromId(channelId, messageId).also {
                        nextToken()
                    }
                }
    }

    private object MessageSerializer : JsonSerializer<Message>() {
        override fun serialize(value: Message?, gen: JsonGenerator, serializers: SerializerProvider) {
            value?.let { message ->
                with(gen) {
                    writeStartObject()
                    writeNumberField("channel", message.channelId.asLong())
                    writeNumberField("message", message.id.asLong())
                    writeEndObject()
                }
            } ?: gen.writeNull()
        }
    }
}