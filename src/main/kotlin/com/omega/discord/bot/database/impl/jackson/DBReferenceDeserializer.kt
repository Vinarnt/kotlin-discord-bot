package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.core.JsonToken
import com.fasterxml.jackson.databind.BeanDescription
import com.fasterxml.jackson.databind.DeserializationConfig
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.deser.BeanDeserializer
import com.fasterxml.jackson.databind.deser.BeanDeserializerModifier
import com.fasterxml.jackson.databind.deser.std.ThrowableDeserializer
import com.mongodb.client.MongoDatabase
import org.bson.types.ObjectId
import org.litote.kmongo.findOneById


class DBReferenceDeserializer(val database: MongoDatabase, deserializer: BeanDeserializer) :
    ThrowableDeserializer(deserializer) {
    override fun deserializeFromObject(jp: JsonParser, ctxt: DeserializationContext): Any? {
        return if (jp.currentName == "\$ref") {
            val ref = jp.nextTextValue()
            jp.nextValue()
            val id = jp.valueAsString
            while (jp.currentToken != JsonToken.END_OBJECT) jp.nextToken()
            database.getCollection(ref).withDocumentClass(_beanType.rawClass).findOneById(ObjectId(id))
        } else {
            super.deserializeFromObject(jp, ctxt)
        }
    }
}

class KMongoBeanDeserializerModifier(val database: MongoDatabase) : BeanDeserializerModifier() {
    override fun modifyDeserializer(
        config: DeserializationConfig,
        beanDesc: BeanDescription,
        deserializer: JsonDeserializer<*>
    ): JsonDeserializer<*> {
        return if (deserializer is BeanDeserializer) {
            DBReferenceDeserializer(database, deserializer)
        } else {
            deserializer
        }
    }
}