package com.omega.discord.bot.database.impl

import com.mongodb.client.MongoDatabase
import com.omega.discord.bot.database.GroupDAO
import com.omega.discord.bot.permission.Group
import discord4j.core.`object`.entity.Guild
import org.bson.types.ObjectId
import org.litote.kmongo.*


class MorphiaGroupDAO(private val database: MongoDatabase) : GroupDAO {
    override fun find(id: ObjectId): Group? = database.getCollection<Group>().findOneById(id)

    override fun findFor(guild: Guild): List<Group> =
        database.getCollection<Group>().find("{guild: ${guild.id.asLong()}}").toList()

    override fun insert(entity: Group): Group =
        entity.also { database.getCollection<Group>().insertOne(entity) }

    override fun update(entity: Group): Group =
        entity.also { database.getCollection<Group>().updateOne(entity) }

    override fun addPermission(entity: Group, permission: String) {
        database.getCollection<Group>()
            .updateOne(
                Group::id eq entity.id,
                addToSet(Group::permissions, permission)
            )
    }

    override fun removePermission(entity: Group, permission: String) {
        database.getCollection<Group>()
            .updateOne(
                Group::id eq entity.id,
                pull(Group::permissions, permission)
            )
    }

    override fun delete(entity: Group) {
        database.getCollection<Group>()
            .deleteOneById(entity.id!!)
    }

    override fun clean() {
    }
}