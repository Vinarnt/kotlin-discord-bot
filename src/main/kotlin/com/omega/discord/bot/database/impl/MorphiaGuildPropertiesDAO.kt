package com.omega.discord.bot.database.impl

import com.mongodb.client.MongoDatabase
import com.omega.discord.bot.database.GuildPropertiesDAO
import com.omega.discord.bot.property.GuildProperties
import com.omega.discord.bot.property.GuildProperty
import discord4j.core.`object`.entity.Guild
import org.bson.types.ObjectId
import org.litote.kmongo.*
import org.litote.kmongo.util.KMongoConfiguration
import org.litote.kmongo.util.idValue


class MorphiaGuildPropertiesDAO(private val database: MongoDatabase) : GuildPropertiesDAO {
    override fun find(id: ObjectId): GuildProperties? =
            database.getCollection<GuildProperties>()
                    .findOneById(id)

    override fun findFor(guild: Guild): GuildProperties? =
            database.getCollection<GuildProperties>()
                    .findOne("{guild: ${guild.id.asLong()}}")

    override fun insert(entity: GuildProperties): GuildProperties =
            entity.also {
                database.getCollection<GuildProperties>()
                        .insertOne(entity)
            }

    override fun update(entity: GuildProperties): GuildProperties =
            entity.also {
                database.getCollection<GuildProperties>()
                        .updateOne(entity)
            }

    override fun updateProperty(entity: GuildProperties, property: GuildProperty, value: Any?): GuildProperties =
            entity.also {
                val serializedValue = KMongoConfiguration.bsonMapperCopy.writeValueAsString(value)
                val filterStr = "{ _id: ObjectId(\"${entity.idValue}\") }"
                val updateStr = "{ \$set: { \"properties.${property.name}.value\": $serializedValue } }"

                database.getCollection<GuildProperties>()
                        .updateOne(
                                filterStr,
                                updateStr
                        )
            }

    override fun delete(entity: GuildProperties) {
        database.getCollection<GuildProperties>()
                .deleteOneById(entity.id!!)
    }

    override fun clean() {
    }
}