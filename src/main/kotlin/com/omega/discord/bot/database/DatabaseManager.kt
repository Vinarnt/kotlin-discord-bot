package com.omega.discord.bot.database

import com.fasterxml.jackson.databind.module.SimpleModule
import com.mongodb.MongoClientSettings
import com.mongodb.MongoCredential
import com.mongodb.ServerAddress
import com.mongodb.client.MongoClient
import com.mongodb.client.MongoDatabase
import com.omega.discord.bot.BotConfiguration
import com.omega.discord.bot.database.impl.MorphiaGroupDAO
import com.omega.discord.bot.database.impl.MorphiaGuildPropertiesDAO
import com.omega.discord.bot.database.impl.MorphiaUserDAO
import com.omega.discord.bot.database.impl.jackson.GuildModule
import com.omega.discord.bot.database.impl.jackson.KMongoBeanDeserializerModifier
import com.omega.discord.bot.database.impl.jackson.MessageModule
import com.omega.discord.bot.database.impl.jackson.RoleModule
import com.omega.discord.bot.database.impl.jackson.UserModule
import org.litote.kmongo.KMongo
import org.litote.kmongo.util.CollectionNameFormatter
import org.litote.kmongo.util.KMongoConfiguration
import org.slf4j.LoggerFactory
import kotlin.reflect.KClass
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.createType
import kotlin.reflect.full.isSupertypeOf


object DatabaseManager {
    private val logger = LoggerFactory.getLogger("Database")
    private val client: MongoClient
    private val database: MongoDatabase
    private val daos = mutableMapOf<KClass<out DAO<*>>, DAO<*>>()

    init {
        val databaseConfig = BotConfiguration.get().database
        val serverAddress = ServerAddress(databaseConfig.host, databaseConfig.port)

        client = if (databaseConfig.user.isNotBlank()) { // Authenticate
            val credential =
                MongoCredential.createCredential(
                    databaseConfig.user,
                    "admin",
                    databaseConfig.password.toCharArray()
                )
            KMongo.createClient(
                MongoClientSettings
                    .builder()
                    .applyToClusterSettings { builder -> builder.hosts(listOf(serverAddress)) }
                    .credential(credential)
                    .build()
            )
        } else {
            KMongo.createClient(
                MongoClientSettings
                    .builder()
                    .applyToClusterSettings { builder -> builder.hosts(listOf(serverAddress)) }
                    .build()
            )
        }
        database = client.getDatabase(databaseConfig.name)

        KMongoConfiguration.registerBsonModule(
            SimpleModule().setDeserializerModifier(
                KMongoBeanDeserializerModifier(
                    database
                )
            )
        )
        KMongoConfiguration.registerBsonModule(GuildModule())
        KMongoConfiguration.registerBsonModule(UserModule())
        KMongoConfiguration.registerBsonModule(RoleModule())
        KMongoConfiguration.registerBsonModule(MessageModule())
        CollectionNameFormatter.useSnakeCaseCollectionNameBuilder()

        logger.info("Initialized successfully")
    }

    val userDAO: UserDAO = MorphiaUserDAO(database)
    val groupDAO: GroupDAO = MorphiaGroupDAO(database)
    val guildPropertiesDAO: GuildPropertiesDAO = MorphiaGuildPropertiesDAO(database)

    fun <T : DAO<*>> addDAO(daoClass: KClass<T>) {
        daoClass.constructors.firstOrNull {
            with(it.parameters) { isNotEmpty() and (first().type.isSupertypeOf(MongoDatabase::class.createType())) }
        }?.call(database).also {
            daos[daoClass] = it as T
            @Suppress("UNCHECKED_CAST")
            daos[it::class.allSuperclasses.first() as KClass<T>] = it
        } ?: throw InstantiationError("No constructor with argument of type com.mongodb.client.MongoDatabase found")
    }

    @Suppress("UNCHECKED_CAST")
    fun <T : DAO<*>> removeDAO(dao: KClass<T>): T? =
        daos.remove(dao) as T?

    @Suppress("UNCHECKED_CAST")
    fun <T : DAO<*>> getDAO(daoClass: KClass<T>): T? = daos[daoClass] as T?

    fun close() {
        userDAO.clean()
        groupDAO.clean()
        guildPropertiesDAO.clean()
        daos.forEach { entry -> entry.value.clean() }
    }
}