package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.module.SimpleModule
import com.omega.discord.bot.getGuildFromId
import discord4j.core.`object`.entity.Guild


class GuildModule : SimpleModule() {
    init {
        addSerializer(Guild::class.java, GuildSerializer)
        addDeserializer(Guild::class.java, GuildDeserializer)
    }

    object GuildDeserializer : JsonDeserializer<Guild>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Guild? {
            return getGuildFromId(p.longValue)
        }
    }

    object GuildSerializer : JsonSerializer<Guild>() {
        override fun serialize(value: Guild?, gen: JsonGenerator, serializers: SerializerProvider) {
            if (value == null) serializers.defaultSerializeNull(gen)
            else gen.writeNumber(value.id.asLong())
        }
    }
}