package com.omega.discord.bot.database.impl.jackson

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.core.JsonParser
import com.fasterxml.jackson.databind.DeserializationContext
import com.fasterxml.jackson.databind.JsonDeserializer
import com.fasterxml.jackson.databind.JsonSerializer
import com.fasterxml.jackson.databind.SerializerProvider
import com.fasterxml.jackson.databind.jsontype.TypeSerializer
import com.fasterxml.jackson.databind.module.SimpleModule
import com.omega.discord.bot.getRoleFromId
import discord4j.core.`object`.entity.Role


class RoleModule : SimpleModule() {
    init {
        addSerializer(Role::class.java, RoleSerializer)
        addDeserializer(Role::class.java, RoleDeserializer)
    }

    private object RoleDeserializer : JsonDeserializer<Role>() {
        override fun deserialize(p: JsonParser, ctxt: DeserializationContext): Role? =
                p.readValueAs(Long::class.java)?.let { getRoleFromId(it) }
    }

    private object RoleSerializer : JsonSerializer<Role>() {
        override fun serialize(value: Role?, gen: JsonGenerator, serializers: SerializerProvider) {
            value?.let { gen.writeNumber(it.id.asLong()) } ?: gen.writeNull()
        }
    }
}