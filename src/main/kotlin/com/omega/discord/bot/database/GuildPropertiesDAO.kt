package com.omega.discord.bot.database

import com.omega.discord.bot.property.GuildProperties
import com.omega.discord.bot.property.GuildProperty
import discord4j.core.`object`.entity.Guild


interface GuildPropertiesDAO : DAO<GuildProperties> {

    fun findFor(guild: Guild): GuildProperties?

    fun updateProperty(entity: GuildProperties, property: GuildProperty, value:Any?): GuildProperties
}