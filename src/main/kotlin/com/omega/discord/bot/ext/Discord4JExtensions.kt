package com.omega.discord.bot.ext

import discord4j.core.`object`.entity.User


fun User.getNameAndDiscriminator(): String = "$username#$discriminator"
