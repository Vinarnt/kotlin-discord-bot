package com.omega.discord.bot.ext

import com.omega.discord.bot.BotConfiguration
import java.net.URI
import java.nio.file.Path
import java.nio.file.Paths

val jarPath: Path by lazy {
    val jarPath: URI = BotConfiguration::class.java.protectionDomain.codeSource.location.toURI()
    if (jarPath.path.contains("build") || jarPath.path.contains("production")) // Launched from IDE
        Paths.get(System.getProperty("user.dir"))
    else
        Paths.get(jarPath).parent
}

val configFilePath: Path by lazy {
    when {
        // Running from an installDist start script
        jarPath.endsWith("lib") -> Paths.get(jarPath.parent.toString(), "bin")
        else -> jarPath
    }.let { Paths.get(it.toString(), "config.yml") }
}