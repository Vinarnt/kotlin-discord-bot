package com.omega.discord.bot.config


data class Database(
    val host: String = "localhost",
    val port: Int = 27017,
    val name: String = "kotlin_discord_bot",
    val user: String = "",
    val password: String = ""
)