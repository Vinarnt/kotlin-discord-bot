package com.omega.discord.bot.config


data class BotConfig(val token: String, val shards: Int = 1, val database: Database)