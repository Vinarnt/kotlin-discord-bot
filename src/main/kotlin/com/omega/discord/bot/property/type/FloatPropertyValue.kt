package com.omega.discord.bot.property.type


class FloatPropertyValue(value: Float = -1f) : PropertyValue<Float>(value)