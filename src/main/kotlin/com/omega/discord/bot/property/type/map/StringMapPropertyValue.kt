package com.omega.discord.bot.property.type.map

import com.omega.discord.bot.property.type.PropertyValue


class StringMapPropertyValue(value: MutableMap<String, String> = mutableMapOf()) : PropertyValue<MutableMap<String, String>>(value)
