package com.omega.discord.bot.property.type.map

import com.omega.discord.bot.property.type.PropertyValue


class MapPropertyValue<K, V>(value: MutableMap<K, V> = mutableMapOf()) : PropertyValue<MutableMap<K, V>>(value)
