package com.omega.discord.bot.property.type


open class PropertyValue<T>(open var value: T)