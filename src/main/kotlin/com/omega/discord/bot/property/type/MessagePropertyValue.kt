package com.omega.discord.bot.property.type

import discord4j.core.`object`.entity.Message


class MessagePropertyValue(value: Message? = null) : PropertyValue<Message?>(value)