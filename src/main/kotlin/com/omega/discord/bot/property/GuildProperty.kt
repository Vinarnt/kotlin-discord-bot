package com.omega.discord.bot.property

import com.omega.discord.bot.property.type.*
import com.omega.discord.bot.property.type.map.StringMapPropertyValue


enum class GuildProperty(val key: String, val defaultValue: PropertyValue<*>) {
    COMMAND_PREFIX("command.prefix", StringPropertyValue("!")),
    AVAILABLE_SELFROLES("available.selfroles", RoleSetPropertyValue()),
    AUTOROLE("autorole", RolePropertyValue()),
    NOTICES("notices", StringMapPropertyValue()),
    AGREEMENT_MESSAGE("agreement.message", MessagePropertyValue()),
    AGREEMENT_ROLE("agreement.role", RolePropertyValue());

    companion object {
        fun get(key: String): GuildProperty? = values().find { it.key.contentEquals(key) }
    }
}