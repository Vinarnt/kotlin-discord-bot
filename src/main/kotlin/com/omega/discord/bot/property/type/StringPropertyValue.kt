package com.omega.discord.bot.property.type


class StringPropertyValue(value: String? = null) : PropertyValue<String?>(value)