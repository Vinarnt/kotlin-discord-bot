package com.omega.discord.bot.property

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.database.DatabaseManager
import com.omega.discord.bot.property.type.PropertyValue
import discord4j.core.`object`.entity.Guild
import discord4j.core.event.domain.guild.GuildCreateEvent
import reactor.core.publisher.Mono


object GuildPropertyManager {
    private val guildPropertiesMap: MutableMap<Guild, GuildProperties> = hashMapOf()

    init {
        BotManager.gateway.on(GuildCreateEvent::class.java) { event ->
            val guild = event.guild
            var guildProperties = DatabaseManager.guildPropertiesDAO.findFor(guild)
            if (guildProperties == null) {
                guildProperties = GuildProperties(guild)
                GuildProperty.values().forEach {
                    guildProperties.set(it, it.defaultValue)
                }
                DatabaseManager.guildPropertiesDAO.insert(guildProperties)
            }
            guildPropertiesMap[guild] = guildProperties
            Mono.empty<GuildCreateEvent>()
        }.subscribe()
    }

    /**
     * Get a property value for a guild.
     *
     * @param guild which guild to get property from
     * @param property the property to get value of
     * @return the property value if found, the property default value otherwise
     */
    fun get(guild: Guild, property: GuildProperty): PropertyValue<*> =
            guildPropertiesMap[guild]!!.get(property)

    /**
     * Set a property for a guild.
     *
     * @param guild which guild to set property to
     * @param property the property to set
     * @param propertyValue the property value to set
     * @return true if value has been set successfully, false otherwise
     */
    fun <T : PropertyValue<*>> set(guild: Guild, property: GuildProperty, propertyValue: T): Boolean {
        guildPropertiesMap[guild]?.let {
            it.set(property, propertyValue)
            DatabaseManager.guildPropertiesDAO.updateProperty(it, property, propertyValue.value)

            return true
        }
        return false
    }
}