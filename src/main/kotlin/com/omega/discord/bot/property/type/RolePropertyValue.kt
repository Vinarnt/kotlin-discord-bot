package com.omega.discord.bot.property.type

import discord4j.core.`object`.entity.Role


class RolePropertyValue(value: Role? = null) : PropertyValue<Role?>(value)