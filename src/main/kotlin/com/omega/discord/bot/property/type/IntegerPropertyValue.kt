package com.omega.discord.bot.property.type


class IntegerPropertyValue(value: Int = -1) : PropertyValue<Int>(value)