package com.omega.discord.bot.property.type

import discord4j.core.`object`.entity.channel.Channel


class ChannelPropertyValue(value: Channel? = null) : PropertyValue<Channel?>(value)