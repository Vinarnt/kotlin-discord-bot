package com.omega.discord.bot.property.type


class BooleanPropertyValue(value: Boolean = false) : PropertyValue<Boolean>(value)