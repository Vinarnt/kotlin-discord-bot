package com.omega.discord.bot.property.type

import com.omega.discord.bot.getRoleFromId
import discord4j.core.`object`.entity.Role


class RoleSetPropertyValue(value: MutableSet<Role> = mutableSetOf()) : PropertyValue<MutableSet<Role>>(value) {
    fun onPostLoad() {
        @Suppress("UNCHECKED_CAST")
        value = (value as MutableList<Long>).mapNotNull {
            getRoleFromId(it)
        }.toMutableSet()
    }
}