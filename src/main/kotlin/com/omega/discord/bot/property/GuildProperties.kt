package com.omega.discord.bot.property

import com.fasterxml.jackson.annotation.JsonPropertyOrder
import com.fasterxml.jackson.annotation.JsonSubTypes
import com.fasterxml.jackson.annotation.JsonTypeInfo
import com.omega.discord.bot.property.type.*
import com.omega.discord.bot.property.type.map.StringMapPropertyValue
import discord4j.core.`object`.entity.Guild
import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId


@JsonPropertyOrder("id", "guild", "properties")
class GuildProperties() {
    @BsonId
    val id: ObjectId? = null

    lateinit var guild: Guild

    @JsonTypeInfo(
            use = JsonTypeInfo.Id.NAME,
            include = JsonTypeInfo.As.PROPERTY,
            property = "type")
    @JsonSubTypes(
            JsonSubTypes.Type(value = StringPropertyValue::class, name = "StringPropertyValue"),
            JsonSubTypes.Type(value = IntegerPropertyValue::class, name = "IntegerPropertyValue"),
            JsonSubTypes.Type(value = FloatPropertyValue::class, name = "FloatPropertyValue"),
            JsonSubTypes.Type(value = BooleanPropertyValue::class, name = "BooleanPropertyValue"),
            JsonSubTypes.Type(value = RolePropertyValue::class, name = "RolePropertyValue"),
            JsonSubTypes.Type(value = RoleSetPropertyValue::class, name = "RolePropertySetValue"),
            JsonSubTypes.Type(value = ChannelPropertyValue::class, name = "ChannelPropertyValue"),
            JsonSubTypes.Type(value = MessagePropertyValue::class, name = "MessagePropertyValue"),
            JsonSubTypes.Type(value = StringMapPropertyValue::class, name = "StringMapPropertyValue")
    )
    val properties: MutableMap<GuildProperty, in PropertyValue<*>?> = mutableMapOf()

    constructor(guild: Guild) : this() {
        this.guild = guild
    }

    fun get(property: GuildProperty): PropertyValue<*> = (properties[property]
            ?: property.defaultValue) as PropertyValue<*>

    fun <T : PropertyValue<*>> set(property: GuildProperty, value: T?) {
        properties[property] = value
    }
}