package com.omega.discord.bot

import com.omega.discord.bot.config.BotConfig
import com.omega.discord.bot.ext.configFilePath
import com.sksamuel.hoplite.ConfigLoader


object BotConfiguration {

    private lateinit var config: BotConfig

    /**
     * Load bot configuration
     */
    fun load() {
        config = ConfigLoader().loadConfigOrThrow(configFilePath.toString())
    }

    fun get(): BotConfig = config
}