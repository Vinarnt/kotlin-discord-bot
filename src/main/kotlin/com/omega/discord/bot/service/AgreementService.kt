package com.omega.discord.bot.service

import com.omega.discord.bot.BotManager
import com.omega.discord.bot.property.GuildProperty
import com.omega.discord.bot.property.GuildPropertyManager
import discord4j.common.util.Snowflake
import discord4j.core.`object`.entity.*
import discord4j.core.`object`.reaction.ReactionEmoji
import discord4j.core.event.domain.guild.GuildCreateEvent
import discord4j.core.event.domain.guild.GuildDeleteEvent
import discord4j.core.event.domain.message.ReactionAddEvent
import discord4j.core.event.domain.message.ReactionRemoveEvent
import org.slf4j.LoggerFactory
import reactor.core.Disposable
import reactor.core.publisher.Mono


object AgreementService {
    const val reaction = "✅"

    private val logger = LoggerFactory.getLogger(AgreementService::class.java)
    private val subscribers = mutableMapOf<Guild, Pair<Disposable, Disposable>>()

    init {
        with(BotManager.gateway.eventDispatcher) {
            on(GuildCreateEvent::class.java)
                    .flatMap { event: GuildCreateEvent ->
                        val guild = event.guild
                        try {
                            addSubscribers(guild)
                        } catch (t: Throwable) {
                            logger.error("Unable to add agreement listener for guild ${guild.name}", t)
                        }
                        Mono.empty<Any>()
                    }
                    .onErrorContinue { t, _ -> logger.error("Guild initialization failed", t) }
                    .subscribe()

            on(GuildDeleteEvent::class.java)
                    .flatMap { event: GuildDeleteEvent ->
                        val guild = event.guild
                        subscribers.remove<Any, Pair<Disposable, Disposable>>(guild)
                        Mono.empty<Any>()
                    }.subscribe()
        }
    }

    fun addSubscribers(guild: Guild) {
        if (subscribers.containsKey(guild))
            return

        with(BotManager.gateway) {
            val addSubscriber = on(ReactionAddEvent::class.java) { event: ReactionAddEvent ->
                doIfEnabled(guild, event.emoji, event.messageId, event.user.block()!!) { member, role ->
                    member.addRole(role.id).block()
                }
                Mono.empty<Any>()
            }.subscribe()
            val removeSubscriber = on(ReactionRemoveEvent::class.java) { event: ReactionRemoveEvent ->
                doIfEnabled(guild, event.emoji, event.messageId, event.user.block()!!) { member, role ->
                    member.removeRole(role.id).block()
                }
                Mono.empty<Any>()
            }.subscribe()

            subscribers[guild] = Pair(addSubscriber, removeSubscriber)
        }
    }

    private fun doIfEnabled(guild: Guild, emoji: ReactionEmoji, messageId: Snowflake, user: User, function: (member: Member, role: Role) -> Unit) {
        val agreementMessage = GuildPropertyManager.get(guild, GuildProperty.AGREEMENT_MESSAGE).value as Message?
        val agreementRole = GuildPropertyManager.get(guild, GuildProperty.AGREEMENT_ROLE).value as Role?
        val reaction = emoji.asUnicodeEmoji().let { if (it.isPresent) it.get().raw else "" }
        if (
                agreementMessage != null
                && agreementRole != null
                && reaction == this@AgreementService.reaction
                && messageId == agreementMessage.id
        ) {
            function(user.asMember(guild.id).block()!!, agreementRole)
        }
    }
}