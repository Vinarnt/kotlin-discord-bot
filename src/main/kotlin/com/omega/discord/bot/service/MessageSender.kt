package com.omega.discord.bot.service

import discord4j.core.`object`.entity.Message
import discord4j.core.`object`.entity.User
import discord4j.core.`object`.entity.channel.MessageChannel
import discord4j.core.spec.EmbedCreateSpec
import discord4j.core.spec.MessageCreateSpec
import reactor.core.publisher.Mono
import java.io.InputStream
import java.util.function.Consumer


object MessageSender {
    fun sendMessage(channel: Mono<MessageChannel>, message: String): Message =
        sendMessage(channel.block()!!, message)

    fun sendMessage(channel: MessageChannel, message: String): Message =
        channel.createMessage(message).block()!!

    fun sendPrivateMessage(to: Mono<User>, message: String): Message =
        sendPrivateMessage(to.block()!!, message)

    fun sendPrivateMessage(to: User, message: String): Message =
        to.privateChannel.flatMap { it.createMessage(message) }.block()!!

    fun sendMessage(channel: Mono<MessageChannel>, embedSpec: EmbedCreateSpec): Message =
        sendMessage(channel.block()!!, embedSpec)

    fun sendMessage(channel: MessageChannel, embedSpec: EmbedCreateSpec): Message =
        channel.createMessage(embedSpec).block()!!

    fun sendMessage(
        channel: Mono<MessageChannel>,
        message: String?,
        fileName: String,
        inputStream: InputStream
    ): Message =
        sendMessage(channel.block()!!, message, fileName, inputStream)

    fun sendMessage(channel: MessageChannel, message: String?, fileName: String, inputStream: InputStream): Message =
        channel.createMessage(
            MessageCreateSpec.builder().apply {
                message?.also { message -> content(message) }
                addFile(fileName, inputStream)
            }.build()
        ).block()!!
}