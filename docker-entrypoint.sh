#!/usr/bin/env bash

set -e

if [[ -z "$DISCORD_TOKEN" ]]; then
    echo "DISCORD_TOKEN environment variable missing"
    exit 1
fi

function render_template() {
  eval "echo \"$(cat $1)\""
}

if [[ ! -f "/usr/app/bin/config.yml" ]]; then
    echo "Generating config file"
    render_template config.yml.template > bin/config.yml
fi

sh bin/kotlin-discord-bot